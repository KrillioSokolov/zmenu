//
//  AppDelegate.swift
//  Menu
//
//  Created by Kirill Sokolov on 28.02.2019.
//  Copyright © 2019 PrivatBank. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private(set) var appCoordinator: AppCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.makeKeyAndVisible()
        
        IQKeyboardManager.shared.enable = true
        application.registerNotifications()
        
        appCoordinator = AppCoordinator(window: window!)
        
        return appCoordinator.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        DispatchQueue.main.async { //This is for correct socket disconnect.
            //self.appCoordinatingController.chatService.applicationDidEnterBackground(application)
        }
        appCoordinator.applicationDidEnterBackground(application)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        appCoordinator.applicationDidBecomeActive(application)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        DispatchQueue.main.async { //This is for correct socket disconnect.
            //  self.appCoordinatingController.chatService.applicationWillEnterForeground(application)
        }
    }
    
}

extension AppDelegate {
    
    // MARK: Notification deep linking
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        guard let userInfo = userInfo as? [String : Any] else { return }
        
        if #available(iOS 10.0, *) {
            appCoordinator.userNotificationCenterHelper.handleNotificationRequest(userInfo, delegate: self)
        } else {
            appCoordinator.userNotificationCenterHelper.handleNotificationRequest(userInfo)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void) {
        guard let userInfo = userInfo as? [String: Any] else { return }
        
        appCoordinator.remoteNotificationHandler?.handleRemoteNotification(userInfo)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        
        appCoordinator.deviceTokenUpdate(deviceToken: token)
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
        
        completionHandler( [.alert, .badge, .sound])
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        guard let userInfo = response.notification.request.content.userInfo as? [String : Any] else { return }
        
        appCoordinator.remoteNotificationHandler?.handleRemoteNotification(userInfo)
    }
    
}


