//
//  TabBarCoordinator.swift
//  Hookers
//
//  Created by Hookers on 5/29/18.
//  Copyright © 2017 Hookers. All rights reserved.
//

import Foundation
import UIKit

enum TabItemIndex: Int {
    
    case restaurants = 0
    case orders = 1
    
}

final class TabBarCoordinator: Coordinator {
    
    fileprivate var root: TabBarController!
    
    override func prepareForStart() {
        super.prepareForStart()
        
        root = makeTabBarController()
    }
    
    override func createFlow() -> TabBarController {
        return root
    }
    
    func addTabCoordinators(coordinators: [TabBarEmbedCoordinator]) {
        var controllers = [UIViewController]()
        var tabItemMap = [(controller: UIViewController, tabItem: UITabBarItem)]()
        for coordinator in coordinators {
            let controller = coordinator.createFlow()
            let tabItem = coordinator.tabItem()
            tabItemMap.append((controller: controller, tabItem: tabItem))
            controllers.append(controller)
        }
        
        root.configureTabs(with: tabItemMap)
    }
    
    func selectTab(_ tabIndex: TabItemIndex) {
        DispatchQueue.updateUI {
            self.root.selectedIndex = tabIndex.rawValue
        }
    }
    
    private func makeTabBarController() -> TabBarController {
        let tabBarController = UIStoryboard.TabBar.tabBarViewController

        return tabBarController
    }
    
}

