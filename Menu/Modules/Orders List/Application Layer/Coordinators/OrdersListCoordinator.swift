//
//  OrdersListCoordinator.swift
//  Menu
//
//  Created by Kirill Sokolov on 25.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class OrdersListCoordinator: TabBarEmbedCoordinator {
    
    fileprivate var root: UINavigationController!
    fileprivate var ordersStore: OrdersListStore!
    
    private var dispatcher: Dispatcher {
        return context.dispatcher
    }
    
    init(context: CoordinatingContext) {
        let servicesImage = UIImage(named: "services")
        let servicesActiveImage = UIImage(named: "services_active")
        
        let tabItemInfo = TabBarItemInfo(
            title: nil,
            icon: servicesImage,
            highlightedIcon: servicesActiveImage)
        
        super.init(context: context, tabItemInfo: tabItemInfo)
    }
    
    override func prepareForStart() {
        super.prepareForStart()
        
        ordersStore = makeOrdersStore()
        openOrdersListViewController()
        register()
    }
    
    override func createFlow() -> UIViewController {
        return root
    }
    
}

extension OrdersListCoordinator {
    
    func makeOrdersStore() -> OrdersListStore {
        let restaurantNetwork = OrdersNetworkService(networkService: context.networkService)
        
        return OrdersListStore(networkService: restaurantNetwork, dispatcher: context.dispatcher, userSessionService: context.userSessionService)
    }
    
}

//MARK: Open View Controllers
extension OrdersListCoordinator {
    
    private func openOrdersListViewController() {
        let ordersListViewController = UIStoryboard.OrdersList.ordersListViewController
        
        ordersListViewController.dispatcher = context.dispatcher
        ordersListViewController.styleguide = context.styleguide
        ordersListViewController.ordersListStore = ordersStore
        
        root = UINavigationController(rootViewController: ordersListViewController)
    }
    
    private func openRateMasterViewController(with order: Order) {
        let rateMasterViewController = UIStoryboard.OrdersList.rateMasterViewController
        
        rateMasterViewController.dispatcher = context.dispatcher
        rateMasterViewController.styleguide = context.styleguide
        rateMasterViewController.ordersListStore = ordersStore
        rateMasterViewController.order = order
        
        let navBarOnModal: UINavigationController = UINavigationController(rootViewController: rateMasterViewController)
        
        root.present(navBarOnModal, animated: true, completion: nil)
    }
    
}

extension OrdersListCoordinator {
    
    private func register() {
        registerCloseScreen()
        registerRateMaster()
    }
    
    private func registerCloseScreen() {
        dispatcher.register(type: OrdersEvent.NavigationEvent.CloseScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                if self?.root.presentedViewController != nil {
                    self?.root.dismiss(animated: box.animated, completion: nil)
                } else {
                    self?.root.popViewController(animated: true)
                }
            case .failure(_):
                break
            }
        }
    }
    
    private func registerRateMaster() {
        dispatcher.register(type: OrdersEvent.NavigationEvent.RateMaster.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openRateMasterViewController(with: box.order)
            case .failure(_):
                break
            }
        }
    }
    
}

