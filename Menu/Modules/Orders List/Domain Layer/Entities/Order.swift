//
//  Order.swift
//  Menu
//
//  Created by Kirill Sokolov on 22.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

enum OrderCondition: String, Codable {
    
    case new
    case workingOn
    case rating
    case rated
    case notRated
    case canceled
    
}

struct Order: Codable {
    
    let orderId: String
    let dueDate: Int
    let condition: OrderCondition
    let peopleCount: String
    let amount: String
    let place: String? = "👤"
    let restaurantName: String
    let client: Client
    let previewMixes: [PreviewMix]
    
    let rating: Double?
    
    let masterName: String
    let masterImageURL: String?
    
    let address: Address
    
}
