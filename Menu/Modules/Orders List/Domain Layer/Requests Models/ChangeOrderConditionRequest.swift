//
//  ChangeOrderConditionRequest.swift
//  Menu
//
//  Created by Kirill Sokolov on 13.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

struct ChangeOrderConditionRequest: Codable {
    
    struct Data: Codable {
        
        let clientId: String
        let newCondition: String
        let orderId: String
        
    }
    
    let data: Data
    
}
