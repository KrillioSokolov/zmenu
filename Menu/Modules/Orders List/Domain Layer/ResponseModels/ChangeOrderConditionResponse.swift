//
//  ChangeOrderConditionResponse.swift
//  Menu
//
//  Created by Kirill Sokolov on 13.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

struct ChangeOrderConditionResponse: Codable {
    
    struct Data: Codable {
        
        let orderId: String
        let newCondition: OrderCondition
        
    }
    
    let data: Data
    
}
