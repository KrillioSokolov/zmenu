//
//  RateMasterResponse.swift
//  Menu
//
//  Created by Kirill Sokolov on 15.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

struct RateMasterResponse: Codable {
    
    struct Data: Codable {
        
        let orderId: String
        let clientId: String
        let rating: String
        
    }
    
    let data: Data
    
}
