//
//  RemoteNotificationResponse.swift
//  Menu
//
//  Created by Kirill Sokolov on 19.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

struct RemoteNotificationResponse: Codable {
    
    let order: Order
    let action: NotificationType
    
}
