//
//  OrdersStore.swift
//  Menu
//
//  Created by Chelak Stas on 11/18/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct OrdersListStoreStateChange: OptionSet, DataStateChange {
    
    let rawValue: Int
    
    static let isLoadingState = OrdersListStoreStateChange(rawValue: Int.bit1)
    static let orders = OrdersListStoreStateChange(rawValue: Int.bit2)
    static let reloadOrdersList = OrdersListStoreStateChange(rawValue: Int.bit3)
    
}

final class OrdersListStore: DomainModel {
    
    private(set) var networkService: OrdersNetwork
    private(set) var dispatcher: Dispatcher
    private(set) var userSessionService: UserSessionService
    
    private(set) var orders: [Order]?
    private(set) var isLoading = false
    
    init(networkService: OrdersNetwork, dispatcher: Dispatcher, userSessionService: UserSessionService) {
        self.networkService = networkService
        self.dispatcher = dispatcher
        self.userSessionService = userSessionService
        
        super.init()
        
        self.register()
    }
    
    func updateUserSessionService() {
        
    }
    
}

extension OrdersListStore {
    
    func rateMaster(rating: Double, orderId: String) {
        guard let clientId = userSessionService.client?.clientId else { return }
        taskDispatcher.dispatch { [weak self] in
            
            self?.networkService.rateMaster(rating: rating, clientId: clientId, orderId: orderId, completion: { (networkErrorResponse, rateMasterResponse) in
                if rateMasterResponse != nil {
                    self?.changeDataStateOf([.reloadOrdersList] as OrdersListStoreStateChange, work: {
                    })
                }
            })
        }
    }
    
    func changeOrderCondition(newCondition: String, orderId: String) {
        guard let clientId = userSessionService.client?.clientId else { return }
        
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            
            self?.networkService.changeOrderCondition(clientId: clientId, orderId: orderId, newCondition: newCondition, completion: { (networkErrorResponse, changeOrderConditionResponse) in
                if changeOrderConditionResponse != nil {
                    self?.changeDataStateOf([.reloadOrdersList, .isLoadingState] as OrdersListStoreStateChange, work: {
                        self?.isLoading = false
                    })
                } else {
                    self?.showErrorMessage()
                }
            })
        }
        
    }
    
    func getOrdersList(clientId: String) {
        startLoading()

        taskDispatcher.dispatch { [weak self] in
            self?.networkService.getOrdersList(сlientId: clientId, completion: { (networkResponse, ordersListResponse) in
                if let ordersListResponse = ordersListResponse {
                    self?.changeDataStateOf([.orders, .isLoadingState] as OrdersListStoreStateChange, work: {
                        self?.isLoading = false
                        
                        self?.orders = ordersListResponse.data.ordersList
                    })
                } else {
                    self?.showErrorMessage()
                }
            })
        }
    }
    
}

extension OrdersListStore {
    
    func startLoading() {
        changeDataStateOf(OrdersListStoreStateChange.isLoadingState) {
            isLoading = true
        }
    }
    
    func showSuccsessMessage(_ message: String) {
        let value = AlertEvent.ShowSuccessAlert.Value(message: message)
        
        dispatcher.dispatch(type: AlertEvent.ShowSuccessAlert.self, result: Result(value: value))
    }
    
    func showErrorMessage(_ message: String? = nil) {
        changeDataStateOf(OrdersListStoreStateChange.isLoadingState) {
            isLoading = false
            
            let value = AlertEvent.ShowErrorAlert.Value(message: message ?? "Проверьте интернет подключение и попробуйте снова 👾".localized())
            
            dispatcher.dispatch(type: AlertEvent.ShowErrorAlert.self, result: Result(value: value))
        }
    }
    
}


extension OrdersListStore {
    
    func register() {
        registerEndMadeOrder()
    }
    
    func registerEndMadeOrder() {
        dispatcher.register(type: AppEvents.OrderEvents.DidMadeOrder.self) { result, _ in
            switch result {
            case .success(_):
                self.changeDataStateOf(OrdersListStoreStateChange.orders, work: {
                    //CS: TODO: Get order from madeOrder request and append here to array
//                    self
                })
                
            case .failure(_):
                break
            }
        }
    }
    
}
