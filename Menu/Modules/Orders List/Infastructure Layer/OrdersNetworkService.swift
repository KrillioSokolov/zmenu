//
//  OrdersNetworkService.swift
//  Menu
//
//  Created by Chelak Stas on 11/18/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

protocol OrdersNetwork {
    
    func getOrdersList(сlientId: String, completion: @escaping ((NetworkResponse?, NetworkOrdersListResponse?) -> Void))
    func changeOrderCondition(clientId: String, orderId: String, newCondition: String, completion: @escaping ((NetworkResponse?, ChangeOrderConditionResponse?) -> Void))
    func rateMaster(rating: Double, clientId: String, orderId: String, completion: @escaping ((NetworkResponse?, RateMasterResponse?) -> Void))
    
}

final class OrdersNetworkService: BaseNetworkService, OrdersNetwork {
    
    let networkService: HTTPNetworkService
    
    init(networkService: HTTPNetworkService) {
        self.networkService = networkService
    }
    
    func rateMaster(rating: Double, clientId: String, orderId: String, completion: @escaping ((NetworkResponse?, RateMasterResponse?) -> Void)) {
        
        let parameters = [ "clientId" : clientId,
                           "rating" : rating,
                           "orderId" : orderId] as Parameters
        
        networkService.executeRequest(endpoint: "rateMaster", method: .post, parameters: parameters, completion: completion)
    }
    
    func changeOrderCondition(clientId: String, orderId: String, newCondition: String, completion: @escaping ((NetworkResponse?, ChangeOrderConditionResponse?) -> Void)) {
        
        let data = ChangeOrderConditionRequest.Data.init(clientId: clientId, newCondition: newCondition, orderId: orderId)
        let request = ChangeOrderConditionRequest(data: data)
        
        networkService.executeRequest(endpoint: "changeOrderCondition", method: .post, parameters: request.dictionary, completion: completion)
    }
    
    func getOrdersList(сlientId: String, completion: @escaping ((NetworkResponse?, NetworkOrdersListResponse?) -> Void)) {
        let parameters = [ "clientId" : сlientId ] as Parameters
        
        networkService.executeRequest(endpoint: "ordersList", method: .post, parameters: parameters, headers: nil, completion: completion)
    }
    
}
