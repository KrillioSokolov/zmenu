//
//  PreviewMixesTableViewService.swift
//  Menu
//
//  Created by Kirill Sokolov on 13.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

import UIKit

protocol PreviewMixesTableViewServiceDelegate: class {
    
    func previewMixesTableViewServiceDidDeleteMix(_ service: PreviewMixesTableViewService, deletedMix item: PreviewMix)
    
}

final class PreviewMixesTableViewService: NSObject {
    
    private weak var delegate: PreviewMixesTableViewServiceDelegate?
    
    private weak var orderItemsTableView: UITableView?
    private var styleguide: DesignStyleGuide
    
    private var cellLeadingImageConstraintConstant: CGFloat = 20
    
    var orderedMixes: [PreviewMix] = []
    var amount: String?
    var dueTime: String?
    
    init(tableView: UITableView, styleguide: DesignStyleGuide) {
        orderItemsTableView = tableView
        self.styleguide = styleguide
    }
    
    func configurate(with delegate: PreviewMixesTableViewServiceDelegate? = nil) {
        orderItemsTableView?.delegate = self
        orderItemsTableView?.dataSource = self
        orderItemsTableView?.registerReusableCell(cellType: OrderItemTableViewCell.self)
        orderItemsTableView?.registerReusableHeaderFooterView(viewType: OrderItemsTableFooterView.self)
        
        self.delegate = delegate
    }
    
    func addMixToOrder(_ mix: PreviewMix) {
        orderedMixes.insert(mix, at: 0)
        orderItemsTableView?.reloadData()
    }
    
    func updateOrderedMixes(_ mixes: [PreviewMix]) {
        orderedMixes = mixes
        orderItemsTableView?.reloadData()
    }
    
    func setCellsConfiguration(leadingImageConstraint: CGFloat = 20) {
        cellLeadingImageConstraintConstant = leadingImageConstraint
    }
    
}

extension PreviewMixesTableViewService: UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderedMixes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath, cellType: OrderItemTableViewCell.self)
        
        let mix = orderedMixes[indexPath.row]
        
        cell.itemNameLabel.text = mix.name
        cell.orderImageView.download(image: mix.imageURL ?? "", placeholderImage: UIImage(named: "z_violet"))
        cell.priceLabel.text = String(mix.price) + " " + MenuViewController.Constants.grn
        cell.delegate = delegate == nil ? nil : self
        cell.leadingImageConstraint.constant = cellLeadingImageConstraintConstant
        cell.refreshUI(withStyleguide: styleguide)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellHeight
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let amount = amount, let dueTime = dueTime, let footerView = tableView.dequeueReusableHeaderFooterView(viewType: OrderItemsTableFooterView.self) {
            footerView.totalLabel.text = "Итого: ".localized() + amount + " " + MenuViewController.Constants.grn
            footerView.timeLabel.text = dueTime
            
            return footerView
        }
        
        return nil
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return Constants.footerHeight
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Удалить".localized()) { (action, indexPath) in
            
            self.removeOrderedMix(by: indexPath)
        }
        
        return [delete]
    }
    
}

extension PreviewMixesTableViewService: OrderItemTableViewCellDelegate {
    
    func removeOrderedMix(by indexPath: IndexPath) {
        let deletedMix = self.orderedMixes[indexPath.row]
        
        self.orderedMixes.remove(at: indexPath.row)
        orderItemsTableView?.deleteRows(at: [indexPath], with: .none)
        
        DispatchQueue.main.async {
            self.delegate?.previewMixesTableViewServiceDidDeleteMix(self, deletedMix: deletedMix)
        }
    }
    
    func orderItemTableViewCellDidTapRemoveButton(_ orderItemTableViewCell: OrderItemTableViewCell) {
        guard let indexPath = orderItemsTableView?.indexPath(for: orderItemTableViewCell) else { return }
        
        removeOrderedMix(by: indexPath)
    }
    
}


extension PreviewMixesTableViewService {
    
    struct Constants {
        
        static let cellHeight = CGFloat(44)
        static let footerHeight = CGFloat(35)
        
    }
    
}
