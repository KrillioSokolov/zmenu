//
//  OrdersListTableViewService.swift
//  Menu
//
//  Created by Kirill Sokolov on 12.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

import UIKit

final class OrdersListTableViewService: NSObject  {
    
    private var orders: [Order] = []
    private var styleguide: DesignStyleGuide!
    private weak var ordersListTableView: UITableView?
    private weak var cellDelegate: OrderListTableViewCellDelegate?
    
    init(tableView: UITableView) {
        ordersListTableView = tableView
    }
    
    func configurate(with styleguide: DesignStyleGuide, cellDelegate: OrderListTableViewCellDelegate? = nil) {
        ordersListTableView?.delegate = self
        ordersListTableView?.dataSource = self
        ordersListTableView?.tableFooterView = UIView()
        ordersListTableView?.separatorStyle = .none
        ordersListTableView?.registerReusableCell(cellType:
            OrderListTableViewCell.self)
        
        self.cellDelegate = cellDelegate
        self.styleguide = styleguide
    }
    
    func updateOrdersList(with newList: [Order]) {
        orders = newList
        ordersListTableView?.reloadData()
    }
    
}

extension OrdersListTableViewService: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath, cellType: OrderListTableViewCell.self)
        
        let order = orders[indexPath.item]
        
        cell.setup(with: order, delegate: cellDelegate, styleguide: styleguide)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


