//
//  OrdersListViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 25.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class OrdersListViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        let attributes = [ NSAttributedString.Key.foregroundColor: UIColor.white ]
        
        refreshControl.attributedTitle = NSAttributedString(string: "Обновляем список заказов...".localized(), attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(getOrdersList), for: .valueChanged)
        
        refreshControl.tintColor = .white
        
        return refreshControl
    }()
    
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var ordersListStore: OrdersListStore! {
        didSet {
            ordersListStore.addDataStateListener(self)
        }
    }
    
    private var service: OrdersListTableViewService!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurateTableView()
        getOrdersList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getOrdersList()
        setupBars()
    }

    private func setupBars() {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "Список заказов".localized()
        
        tabBarController?.tabBar.isHidden = false
    }
    
}

//MARK: - Configurate
extension OrdersListViewController {
    
    private func configurateTableView() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        service = OrdersListTableViewService(tableView: tableView)
        service.configurate(with: styleguide, cellDelegate: self)
    }
    
    @objc private func getOrdersList() {
        guard let clientId = ordersListStore.userSessionService.client?.clientId else {
            refreshControl.endRefreshing()
            handleEmptyListItems()
            return
        }
        
        ordersListStore.getOrdersList(clientId: clientId)
    }
    
    private func handleEmptyListItems(_ items: [Order]? = nil) {
        guard let items = items else {
             tableView?.showEmptyListMessage("Сделайте свой первый заказ и мы отобразим его здесь 🤗", textColor: .white)
            return
        }
        
        if items.count == 0 {
            tableView?.showEmptyListMessage("Список заказов пуст \nПотяните вниз, что бы обновить 👆", textColor: .white)
        } else {
            tableView?.showEmptyListMessage("", textColor: .white)
        }
    }
    
}

extension OrdersListViewController: OrderListTableViewCellDelegate {
    
    func orderListTableViewCell(_ cell: OrderListTableViewCell, wantsToCancelOrder order: Order) {
        showCancelOrderAlert(orderId: order.orderId, dueDate: order.dueDate, restaurantName: order.restaurantName)
    }
    
    func orderListTableViewCell(_ cell: OrderListTableViewCell, wantsToRateOrder order: Order) {
        let value = OrdersEvent.NavigationEvent.RateMaster.Value(order: order)
        
        dispatcher.dispatch(type: OrdersEvent.NavigationEvent.RateMaster.self, result: Result(value: value))
    }
    
}

extension OrdersListViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? OrdersListStoreStateChange {
            ordersListStoreStateChange(change: change)
        }
    }
    
    private func ordersListStoreStateChange(change: OrdersListStoreStateChange) {
        if change.contains(.isLoadingState) {
            if !ordersListStore.isLoading {
                self.refreshControl.endRefreshing()
                self.hideSpinner()
            } else {
                guard !isSpinerPresented else { return }
                
                self.refreshControl.beginRefreshingManually()
            }
            
            //KS: TODO: Show/hide skeleton
            //restaurantStore.isLoading ? addSkeletonViewController() : hideSkeletonViewController()
        }

        if change.contains(.orders) {
            guard let orders = ordersListStore.orders else { return }

            service.updateOrdersList(with: orders)
            handleEmptyListItems(orders)
        }
        
        if change.contains(.reloadOrdersList) {
            getOrdersList()
        }
        
    }
    
}

extension OrdersListViewController {
    
    @objc func showCancelOrderAlert(orderId: String, dueDate: Int, restaurantName: String) {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите отменить заказ в \(restaurantName) на \(Date.displayableSectionDate(from: Date(seconds: Int64(dueDate))))?".localized(), preferredStyle: .alert)
        
        let attributedTitle = NSAttributedString(string: "Отмена заказа".localized(), attributes:  [NSAttributedString.Key.font: styleguide.boldFont(ofSize: 17)])
        
        alert.setValue(attributedTitle, forKey: "attributedTitle")
        
        let cancelAction = UIAlertAction(title: "Нет".localized(), style: .cancel)
        
        let leaveChannelAction = UIAlertAction(title: "Да, отменить".localized(), style: .default, handler: { _ in
            self.ordersListStore.changeOrderCondition(newCondition: OrderCondition.canceled.rawValue, orderId: orderId)
            self.showSpinner()
        })
        
        leaveChannelAction.setValue(styleguide.errorColor, forKey: "titleTextColor")
        
        [leaveChannelAction, cancelAction].forEach{alert.addAction($0)}
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
