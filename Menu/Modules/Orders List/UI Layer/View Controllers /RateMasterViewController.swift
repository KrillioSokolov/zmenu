//
//  RateMasterViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 15.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import UIKit

final class RateMasterViewController: UIViewController {

    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var ordersListStore: OrdersListStore!
    var order: Order!
    
    @IBOutlet weak var suggestionLabel: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cosmosView.didFinishTouchingCosmos = { rating in
            self.rateMaster(rating: rating)
        }
        
        configurateNavBar()
    }
    
    private func configurateNavBar() {
        navigationItem.addCloseButton(with: self, action: #selector(close), tintColor: styleguide.primaryColor)
        navigationController?.navigationBar.barStyle = .blackOpaque
        navigationItem.title = "Оценка обслуживания".localized()
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
    }
    
    private func rateMaster(rating: Double) {
         ordersListStore.rateMaster(rating: rating, orderId: order.orderId)
        ordersListStore.showSuccsessMessage("Спасибо за Ваше мнение 😋".localized())
        
        DispatchQueue.delay(delay: 0.5) {
            self.close()
        }
    }

    @objc private func close() {
        let value = OrdersEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: OrdersEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
}
