//
//  OrderListTableViewCell.swift
//  Menu
//
//  Created by Kirill Sokolov on 12.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol OrderListTableViewCellDelegate: class {
    
    func orderListTableViewCell(_ cell: OrderListTableViewCell, wantsToCancelOrder order: Order)
    func orderListTableViewCell(_ cell: OrderListTableViewCell, wantsToRateOrder order: Order)
    
}

private struct LocalizedStringConstants {
    
    static var onTheTable: String { return "За столом: ".localized() }
    
}

final class OrderListTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    
    var tableViewService: PreviewMixesTableViewService!
    weak var delegate: OrderListTableViewCellDelegate?
    private var order: Order!
    
    var primaryContactFullAddress: String? {
        return order.address.country + ", " + order.address.city + ", " + order.address.street  + " " + order.address.house
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setup(with order: Order, delegate: OrderListTableViewCellDelegate?, styleguide: DesignStyleGuide) {
        self.order = order
        
        if tableViewService == nil {
            tableViewService = PreviewMixesTableViewService(tableView: tableView, styleguide: styleguide)
            tableViewService.configurate()
            tableViewService.setCellsConfiguration(leadingImageConstraint: 5)
            
            actionButton.addTarget(self, action: #selector(orderAction), for: .touchUpInside)
            
            addressButton.titleLabel?.lineBreakMode = .byWordWrapping
            addressButton.titleLabel?.numberOfLines = 0
        }
        
        tableViewService.amount = order.amount
        tableViewService.dueTime = "Заказ на ".localized() + Date.displayableSectionDate(from: Date(seconds: Int64(order.dueDate)))
        
        tableViewHeightConstraint.constant = CGFloat(order.previewMixes.count) * OrderItemsTableViewService.Constants.cellHeight + OrderItemsTableViewService.Constants.footerHeight
        
        configurate(with: order, styleguide: styleguide)
        tableViewService.updateOrderedMixes(order.previewMixes)
        refreshUI(withStyleguide: styleguide)
        
        self.delegate = delegate
    }
    
    @IBAction func routToTheAddress(_ sender: Any) {
        let testURL: NSURL = NSURL(string: "comgooglemaps-x-callback://")!
        if UIApplication.shared.canOpenURL(testURL as URL) {
            if let address = primaryContactFullAddress?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                let directionsRequest: String = "comgooglemaps-x-callback://" + "?daddr=\(address)" + "&x-success=sourceapp://?resume=true&x-source=AirApp"
                let directionsURL: NSURL = NSURL(string: directionsRequest)!
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(directionsURL as URL)) {
                    if #available(iOS 10.0, *) {
                        application.open(directionsURL as URL, options: [:], completionHandler: nil)
                    } else {
                        
                    }
                }
            }
        } else {
            NSLog("Can't use comgooglemaps-x-callback:// on this device.")
        }
    }
    
    private func configurate(with order: Order, styleguide: DesignStyleGuide) {
         placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
        
        switch order.condition {
        case .new:
            actionButton.isEnabled = true
            
            actionButton.tintColor = styleguide.badgeColor
            actionButton.setTitle("Отменить ❌".localized(), for: .normal)
            
            let conditionEmodje = order.place == nil ? "⏳ " : "📝 "
            
            restaurantNameLabel.text =  conditionEmodje + order.restaurantName
        case .workingOn:
            actionButton.isEnabled = true
            
            actionButton.tintColor = styleguide.badgeColor
            actionButton.setTitle("Отменить ❌".localized(), for: .normal)
            restaurantNameLabel.text =  "💡 " + order.restaurantName
        case .rating:
            actionButton.isEnabled = true
            
            actionButton.tintColor = styleguide.warningColor
            actionButton.setTitle("Оценить ⭐️".localized(), for: .normal)
            
            restaurantNameLabel.text = "✅ " + order.restaurantName
        case .rated:
            actionButton.isEnabled = false
            let title: String
            
            if let rating = order.rating {
                let ceilRating = rating.rounded(.down)
                let part = rating - ceilRating
                let ceilStars = String(repeating: "⭐️", count: Int(ceilRating))
                let halfStar = part > 0 ? String(repeating: "✨", count: 1) : ""
                
                title = ceilStars + halfStar
            } else {
                title = "Оценен".localized()
            }
            
            actionButton.setTitle(title, for: .disabled)
            actionButton.tintColor = styleguide.warningColor
            
            restaurantNameLabel.text = "✅ " + order.restaurantName
        case .canceled:
             actionButton.isEnabled = false
             
             actionButton.setTitle("Отменен 🤷‍♂️".localized(), for: .disabled)
             actionButton.tintColor = styleguide.senderTextColor
             restaurantNameLabel.text = "❌ " + order.restaurantName
        case .notRated:
            actionButton.isEnabled = false

            actionButton.setTitle("Без оценки 😒".localized(), for: .disabled)
            actionButton.tintColor = styleguide.senderTextColor
            restaurantNameLabel.text = "✅ " + order.restaurantName
        }
        
        let attributes = [ kCTUnderlineStyleAttributeName : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor.rawValue: UIColor.white] as? [NSAttributedString.Key : Any]
        
        var addressString = order.address.street + " " + order.address.house
        
        if let floor = order.address.floor {
            addressString = addressString + ", " + "этаж ".localized() + floor
        }
        
        addressButton.setAttributedTitle(NSAttributedString(string: addressString, attributes: attributes), for: .normal)
    }
    
    @objc private func orderAction() {
        switch order.condition {
        case .new, .workingOn:
            delegate?.orderListTableViewCell(self, wantsToCancelOrder: order)
        case .rating:
            delegate?.orderListTableViewCell(self, wantsToRateOrder: order)
        default:
            break
        }
    }
    
}

extension OrderListTableViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        containerView.layer.cornerRadius = styleguide.cornerRadius
        containerView.backgroundColor = styleguide.bubbleColor
        tableView.backgroundView = nil
        tableView.backgroundColor = .clear
        tableView.layer.cornerRadius = styleguide.cornerRadius
    }
    
}
