//
//  OrderItemsTableFooterView.swift
//  Menu
//
//  Created by Kirill Sokolov on 13.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import UIKit

final class OrderItemsTableFooterView: UITableViewHeaderFooterView, NibLoadable, NibReusable {

    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
}
