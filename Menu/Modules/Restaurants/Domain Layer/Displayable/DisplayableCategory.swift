//
//  DisplayableCategory.swift
//  Menu
//
//  Created by Kirill Sokolov on 22.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

final class DisplayableCategory: Hashable {
    
    let categoryId: String
    let name: String
    let imageURL: String
    
    var hashValue: Int {
        return categoryId.hashValue
    }
    
    init(categoryId: String, name: String, imageURL: String) {
        self.categoryId = categoryId
        self.name = name
        self.imageURL = imageURL
    }
    
    static func == (lhs: DisplayableCategory, rhs: DisplayableCategory) -> Bool {
        return lhs.categoryId == rhs.categoryId
    }
    
}
