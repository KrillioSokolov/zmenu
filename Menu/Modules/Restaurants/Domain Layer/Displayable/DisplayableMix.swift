//
//  DisplayableMix.swift
//  Menu
//
//  Created by Stas Chelak on 12/7/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct DisplayableMix {
    
    let mixId: String
    let name: String
    let imageURL: String?
    let price: Double
    let categoryId: String
    let restaurantId: String
    var strength: MixLevel
    var likes: String
    var flawor: [Mix.Flawor]
    var description: String
    let bowl: String?
    let filling: String?
    var isFlipped = true
    
    //CS: TODO: localized
    var descriptionStrings: [String] {
        let tobacos = flawor.map({ $0.brand + "(" + $0.sort  + ")" }).joined(separator: ", ")
        
        return [
            "\("Вкусы:".localized()) \(tobacos)",
            "\("Крепкость:".localized()) \(MixLevel.displayableFlaworLevel(strength))",
            "\("В колбе:".localized()) \(filling ?? "Вода".localized())",
            "\("Чаша:".localized()) \(bowl ?? "Силикон".localized())",
            "\("Описание:".localized()) \(description)",
        ]
    }
    
    init(from mix: Mix) {
        mixId = mix.mixId
        name = mix.name
        imageURL = mix.imageURL
        price = mix.price
        categoryId = mix.categoryId
        restaurantId = mix.restaurantId
        strength = mix.strength
        likes = mix.likes
        flawor = mix.flawor
        description = mix.description
        bowl = mix.bowl
        filling = mix.filling
    }
    
}
