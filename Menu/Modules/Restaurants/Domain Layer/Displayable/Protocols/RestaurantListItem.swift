//
//  RestaurantListItem.swift
//  Menu
//
//  Created by Kirill Sokolov on 27.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol RestaurantListItem: class {
    
    var name: String { get }
    var restaurantId: String { get }
    
}
