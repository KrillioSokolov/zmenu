//
//  Master.swift
//  Menu
//
//  Created by Kirill Sokolov on 22.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

final class Master: Codable, RestaurantListItem {
    
    var name: String
    let imageURL: String?
    var rating: String
    let description: String?
    let masterId: String
    let restaurant: Restaurant
    let instagram: String?
    let averageRate: String
    let ordersCount: String
    
    
    var restaurantId: String {
        return restaurant.restaurantId
    }

}
