//
//  Mix.swift
//  Menu
//
//  Created by Kirill Sokolov on 22.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

enum MixLevel: String, Codable {
    
    case light
    case semiLight
    case middle
    case heavy
    case superHeavy
    
    static func displayableFlaworLevel(_ level: MixLevel?) -> String {
        
        switch level {
        case .light?:
            return "Легкий".localized()
        case .semiLight?:
            return "Средний".localized()
        case .middle?:
            return "Полутяжелый".localized()
        case .heavy?:
            return "Тяжелый".localized()
        case superHeavy?:
            return "Очень тяжелый".localized()
        default:
            return "Средний".localized()
        }
        
    }
    
}


struct Mix: Codable {
    
    struct Flawor: Codable {
        
        let brand: String
        let sort: String
        
    }
    
    struct Master: Codable {
        
        let masterId: String
        let name: String
        let imageURL: String?
        
    }
    
    let mixId: String
    let name: String
    var imageURL: String?
    let price: Double
    let categoryId: String
    let restaurantId: String
    var strength: MixLevel
    var likes: String
    var flawor: [Mix.Flawor]
    var description: String
    let bowl: String?
    let filling: String?

}


struct PreviewMix: Codable {
    
    let mixId: String
    let name: String
    let price: String
    var imageURL: String?
    
}
