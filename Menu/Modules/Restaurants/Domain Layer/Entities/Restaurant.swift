//
//  Restaurant.swift
//  Menu
//
//  Created by Kirill Sokolov on 20.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

final class Restaurant: RestaurantListItem, Codable {
    
    let restaurantId: String
    var name: String
    var rating: String
    let address: Address
    let workTimeDescription: String?
    let description: String?
    let instagram: String?
    var photos: [String]
    
}
