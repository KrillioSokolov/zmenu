//
//  RestaurantsEvent.swift
//  Menu
//
//  Created by Kirill Sokolov on 30.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct RestaurantsEvent {
    
    struct NavigationEvent {
        
        struct CloseScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct DidChooseRestaurant: Event {
            typealias Payload = Value
            
            struct Value {
                let restaurant: RestaurantListItem
            }
        }
        
        struct DidTapInfoButtonOnRestaurantCell: Event {
            typealias Payload = Value
            
            struct Value {
                let restaurant: Restaurant
            }
        }
        
        struct DidTapInfoButtonOnMasterCell: Event {
            typealias Payload = Value
            
            struct Value {
                let master: Master
            }
        }
        
        struct DidChooseMixesForOrder: Event {
            typealias Payload = Value
            
            struct Value {
                let restaurant: RestaurantListItem
                let mixesForOrder: [DisplayableMix]
            }
        }
        
        struct DidSelectDueDate: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct ShowLoginController: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct ShowCodeVerificationController: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct ShowPrivacyController: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
    }
    
}
