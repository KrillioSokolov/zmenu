//
//  RestaurantStore.swift
//  Menu
//
//  Created by Kirill Sokolov on 25.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct RestaurantStoreStateChange: OptionSet, DataStateChange {
    
    let rawValue: Int
    
    static let isLoadingState = RestaurantStoreStateChange(rawValue: Int.bit1)
    static let restaurants = RestaurantStoreStateChange(rawValue: Int.bit2)
    static let menu = RestaurantStoreStateChange(rawValue: Int.bit3)
    static let bestMasters = RestaurantStoreStateChange(rawValue: Int.bit4)
    static let mastersForRestaurant = RestaurantStoreStateChange(rawValue: Int.bit5)
    static let otp = RestaurantStoreStateChange(rawValue: Int.bit6)
    
}

final class RestaurantStore: DomainModel {
    
    private(set) var userSessionService: UserSessionService
    private(set) var networkService: RestaurantNetwork
    private(set) var dispatcher: Dispatcher
    
    private(set) var restaurants: [Restaurant]?
    private(set) var menuData: MenuResponse.Data?
    private(set) var mastersData: MastersListResponse.Data?
    private(set) var orderResponseData: OrderResponse.Data?
    
    private(set) var orderRequestModel = OrderRequest()
    private(set) var dueDate: Date?
    private var deviceToken: String?

    private(set) var isLoading = false
    
    init(networkService: RestaurantNetwork, dispatcher: Dispatcher, userSessionService: UserSessionService) {
        self.networkService = networkService
        self.dispatcher = dispatcher
        self.userSessionService = userSessionService
    }
    
}

// MARK: - Actions
extension RestaurantStore {
    
    func updateOrderedMixes(with orderedIds: [(mixId: String, mixCategoryId: String)]) {
        guard let categories = menuData?.categories else { return }

        orderRequestModel.mixes = orderedIds.compactMap { (orderedMix) -> Mix? in
            return categories.first(where: {$0.categoryId == orderedMix.mixCategoryId})?.mixes.first(where: {$0.mixId == orderedMix.mixId})
        }
    }
    
    func didChooseRestaurantListItem(restaurantListItem: RestaurantListItem) {
        orderRequestModel.restaurantId = restaurantListItem.restaurantId
    }
    
    func didSelectDueDate(with dueDate: Date?) {
        guard let dueDate = dueDate else { return }
        
        orderRequestModel.dueDate = dueDate.timeIntervalSince1970
        self.dueDate = dueDate
    }
    
    func resetRequestModel() {
        orderRequestModel = OrderRequest()
    }
    
    func addPlaceDescription(_ place: String?) {
        guard let place = place else { return }
        
        orderRequestModel.place = place
    }
    
    func makeOrderIfPossible(with masterId: String?, peopleCount: String, comment: String) -> Bool {
        orderRequestModel.masterId = masterId
        orderRequestModel.peopleCount = peopleCount
        orderRequestModel.comment = comment
        
        if let client = userSessionService.client {
            orderRequestModel.clientName = client.name
            orderRequestModel.phoneNumber = client.phone
            orderRequestModel.clientId = client.clientId
            makeOrder()
            
            return true
        }
        
        return false
    }
    
    func newClientMakeOrder(phone: String, name: String) {
        orderRequestModel.phoneNumber = phone
        orderRequestModel.clientName = name
        makeOrder()
    }
    
}

//MARK: - Requests
extension RestaurantStore {
    
    func getRestaurantsList() {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.getRestaurantList(dueDate: self?.orderRequestModel.dueDate) { (networkResponse, restaurantListResponse) in
                if let restaurantListResponse = restaurantListResponse {
                    self?.changeDataStateOf([.restaurants, .isLoadingState] as RestaurantStoreStateChange, work: {
                        self?.isLoading = false
                        
                        self?.restaurants = restaurantListResponse.data.restaurants
                    })
                } else {
                    self?.showErrorMessage()
                }
            }
        }
    }
    
    func getMenu(for restaurantListItem: RestaurantListItem) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.getMenu(byRestaurantId: restaurantListItem.restaurantId) { (networkResponse, MenuResponse) in
                
                if let MenuResponse = MenuResponse {
                    self?.changeDataStateOf([.menu, .isLoadingState] as RestaurantStoreStateChange, work: {
                        self?.isLoading = false
                        
                        self?.menuData = MenuResponse.data
                    })
                } else {
                    self?.showErrorMessage()
                }
            }
        }
    }
    
    func deviceTokenUpdate(deviceToken: String) {
        guard let clientId = userSessionService.client?.clientId else {
            self.deviceToken = deviceToken
            
            return
        }
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.deviceTokenUpdate(deviceToken: deviceToken, clientId: clientId, completion: { (networkErrorResponse, deviceTokenUpdate) in
                
            })
        }
    }
    
    func getMastersList(restaurantId: String? = nil) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            
            self?.networkService.getMastersList(byRestaurantId: restaurantId, dueDate: self?.orderRequestModel.dueDate) { (networkReponse, MastersListResponse) in
                let changedType: RestaurantStoreStateChange = restaurantId != nil ? .mastersForRestaurant : .bestMasters
                
                if let MastersListResponse =  MastersListResponse {
                    
                    
                    self?.changeDataStateOf([changedType, .isLoadingState] as RestaurantStoreStateChange, work: {
                        self?.isLoading = false
                        self?.mastersData = MastersListResponse.data
                    })
                } else {
                    self?.changeDataStateOf([changedType] as RestaurantStoreStateChange, work: {
                        self?.showErrorMessage()
                    })
                }
            }
        }
    }

    
    private func makeOrder() {
        orderRequestModel.amount = String(orderRequestModel.mixes.map({$0.price}).reduce(0, +))
        
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.makeOrder(self?.orderRequestModel ?? OrderRequest(), with: { (error, OrderResponse) in
                guard let response = OrderResponse else {
                    self?.showErrorMessage(error?.serverError?.errorText)
                    return
                }
                
                if response.data.expires != nil {
                    self?.changeDataStateOf([.otp, .isLoadingState] as RestaurantStoreStateChange, work: {
                        self?.isLoading = false
                    })
                } else {
                    self?.changeDataStateOf([.isLoadingState] as RestaurantStoreStateChange, work: {
                        self?.isLoading = false
                        
                        let message = "Спасибо за заказ, (X)!\nАдминистратор свяжется с вами в течении 10 мин.".localized().replacingOccurrences(of: "(X)", with: self?.userSessionService.client?.name ?? "")
                        self?.showSuccessMessage(message)
                        self?.resetRequestModel()
                        
                        let value = AppEvents.OrderEvents.DidMadeOrder.Value()
                        self?.dispatcher.dispatch(type: AppEvents.OrderEvents.DidMadeOrder.self, result: Result(value: value))
                    })
                }
            })
        }
    }
    
}


// MARK: - Auth
extension RestaurantStore {
    
    func checkAuth(code: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.check(code: code, for: self?.orderRequestModel.phoneNumber ?? "", completion: { (error, verificationCodeResponse) in
                guard let response = verificationCodeResponse else {
                    self?.showErrorMessage(error?.serverError?.errorText)
                    return
                }
                
                self?.changeDataStateOf([.isLoadingState] as RestaurantStoreStateChange, work: {
                    self?.isLoading = false
                    
                    self?.userSessionService.createUser(clientId: response.data.clientId, phone: self?.orderRequestModel.phoneNumber ?? "", name: self?.orderRequestModel.clientName ?? "")
                    
                    let message = "Спасибо за заказ, (X)!\nАдминистратор свяжется с вами в течении 10 мин.".localized().replacingOccurrences(of: "(X)", with: self?.userSessionService.client?.name ?? "")
                    self?.showSuccessMessage(message)
                    
                    if let deviceToken = self?.deviceToken {
                        self?.deviceTokenUpdate(deviceToken: deviceToken)
                    }
                    
                    let value = AppEvents.OrderEvents.DidMadeOrder.Value()
                    self?.dispatcher.dispatch(type: AppEvents.OrderEvents.DidMadeOrder.self, result: Result(value: value))
                })
            })
        }
    }
    
    func resendAuthCode() {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.resendCode(to: self?.orderRequestModel.phoneNumber ?? "", completion: { (error, resendCodeResponse) in
                
                if error != nil {
                    self?.showErrorMessage(error?.serverError?.errorText)
                } else {
                    self?.changeDataStateOf(RestaurantStoreStateChange.isLoadingState, work: {
                        self?.isLoading = false
                    })
                }
            })
        }
    }
    
}

extension RestaurantStore {
    
    private func startLoading() {
        changeDataStateOf(RestaurantStoreStateChange.isLoadingState) {
            isLoading = true
        }
    }
    
    func showSuccessMessage(_ message: String) {
        let value = AlertEvent.ShowSuccessAlert.Value(message: message)
        
        dispatcher.dispatch(type: AlertEvent.ShowSuccessAlert.self, result: Result(value: value))
    }
    
    func showErrorMessage(_ message: String? = nil) {
        changeDataStateOf(RestaurantStoreStateChange.isLoadingState) {
            isLoading = false
            
            let value = AlertEvent.ShowErrorAlert.Value(message: message ?? "Проверьте интернет подключение и попробуйте снова 👾".localized())
            
            dispatcher.dispatch(type: AlertEvent.ShowErrorAlert.self, result: Result(value: value))
        }
    }
        
}
