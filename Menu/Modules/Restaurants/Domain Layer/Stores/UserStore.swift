//
//  UserSessionService.swift
//  Menu
//
//  Created by Stas Chelak on 2/5/19.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

fileprivate struct Keys {
    static let userData = "UserDefaultsMangerUserDataStringKey"
}

final class UserSessionService {
    
    //KS: TODO: This is temp solution, should be refactored. 
    
    lazy var client: Client? = {
        guard let data = UserDefaults.standard.data(forKey: Keys.userData), let client = NSKeyedUnarchiver.unarchiveObject(with: data) as? Client else { return nil }
        
        return client
    }()
    
    func createUser(clientId: String, phone: String, name: String) {
        let client = Client(name: name, phone: phone, clientId: clientId)
        let data = NSKeyedArchiver.archivedData(withRootObject: client)
        
        UserDefaults.standard.setValue(data, forKey: Keys.userData)
        
        self.client = client
    }

}
