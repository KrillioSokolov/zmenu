//
//  MakeOrderRequest.swift
//  Menu
//
//  Created by Stas Chelak on 2/5/19.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

struct OrderRequest: Codable {
    
    var clientId: String?
    var amount: String?
    var clientName: String?
    var peopleCount: String?
    var masterId: String?
    var mixes: [Mix] = []
    var phoneNumber: String?
    var tableNumber: Int?
    var restaurantId: String?
    var dueDate: Double?
    var comment: String?
    var place: String = "👤"
    
}
