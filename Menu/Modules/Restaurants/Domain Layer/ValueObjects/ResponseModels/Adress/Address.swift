//
//  Address.swift
//  Menu
//
//  Created by Kirill Sokolov on 20.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct Address: Codable {

    let city: String
    let country: String
    let street: String
    let house: String
    let floor: String?
    
    
}
