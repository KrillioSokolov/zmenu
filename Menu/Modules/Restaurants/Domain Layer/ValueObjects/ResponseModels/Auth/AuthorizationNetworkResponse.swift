//
//  AuthorizationNetworkResponse.swift
//  Menu
//
//  Created by Chelak Stas on 12/17/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct AuthorizationNetworkResponse: Decodable {
    
    struct Data: Decodable {
        let state: String
        let clientId: String
    }
    
    let reqId: String
    let action: String
    let data: AuthorizationNetworkResponse.Data
}

