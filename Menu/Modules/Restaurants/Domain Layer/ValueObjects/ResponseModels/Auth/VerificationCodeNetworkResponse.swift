//
//  VerificationCodeNetworkResponse.swift
//  Menu
//
//  Created by Chelak Stas on 12/18/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct VerificationCodeNetworkResponse: Decodable {
    
    struct Data: Decodable {
        let clientId: String
        let orderId: String
    }
    
    let reqId: String
    let action: String
    let data: VerificationCodeNetworkResponse.Data
    
}
