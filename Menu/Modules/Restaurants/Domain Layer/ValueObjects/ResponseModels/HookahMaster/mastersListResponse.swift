//
//  MasterListResponse.swift
//  Menu
//
//  Created by Kirill Sokolov on 31.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct MastersListResponse: Decodable {

    struct Data: Decodable {
        let restaurantId: String?
        let masters: [Master]
    }
    
    let reqId: String
    let action: String
    let data: MastersListResponse.Data
    
}
