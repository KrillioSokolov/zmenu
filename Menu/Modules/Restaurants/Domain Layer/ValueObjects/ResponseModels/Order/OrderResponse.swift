//
//  OrderResponse.swift
//  Menu
//
//  Created by Kirill Sokolov on 04.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct OrderResponse: Codable  {
    
    struct Data: Codable {
        
        let expires: String?
        let orderId: String?
        
    }
    
    let reqId: String
    let action: String
    let data: OrderResponse.Data
    
}
