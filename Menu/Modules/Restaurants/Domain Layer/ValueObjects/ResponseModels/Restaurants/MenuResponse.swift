//
//  MenuResponse.swift
//  Menu
//
//  Created by Kirill Sokolov on 22.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct MenuResponse: Decodable {
    
    struct Data: Decodable {
        let restaurantId: String
        let categories : [MixCategory]
    }
    
    let reqId: String
    let action: String
    let data: MenuResponse.Data
    
}
