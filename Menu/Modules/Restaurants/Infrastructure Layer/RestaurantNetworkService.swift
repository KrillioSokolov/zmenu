//
//  RestaurantNetworkService.swift
//  Menu
//
//  Created by Kirill Sokolov on 25.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation


protocol RestaurantNetwork {
    
    func getRestaurantList(dueDate: Double?, with completion: @escaping ((NetworkResponse?, NetworkRestaurantsListResponse?) -> Void))
    func getMenu(byRestaurantId restaurantId: String, with completion: @escaping ((NetworkResponse?, MenuResponse?) -> Void))
    func getMastersList(byRestaurantId restaurantId: String?, dueDate: Double?, with completion: @escaping ((NetworkResponse?, MastersListResponse?) -> Void))
    func makeOrder(_ order: OrderRequest, with completion: @escaping ((NetworkResponse?, OrderResponse?) -> Void))
    func check(code: String, for phone: String, completion: @escaping ((NetworkResponse?, VerificationCodeNetworkResponse?) -> Void))
    func resendCode(to phone: String, completion: @escaping ((NetworkResponse?, ResendVerificationCodeNetworkResponse?) -> Void))
    func deviceTokenUpdate(deviceToken: String, clientId: String, completion: @escaping ((NetworkResponse?, DeviceTokenUpdateResponse?) -> Void))
    
}

final class RestaurantNetworkService: BaseNetworkService, RestaurantNetwork {
    
    let networkService: HTTPNetworkService
    
    init(networkService: HTTPNetworkService) {
        self.networkService = networkService
    }
    
    func getRestaurantList(dueDate: Double?, with completion: @escaping ((NetworkResponse?, NetworkRestaurantsListResponse?) -> Void)) {
        var parametrs: [String: Any] = [:]
        
        if let dueDate = dueDate {
            parametrs.updateValue(dueDate, forKey: "dueDate")
        }
    
        networkService.executeRequest(endpoint: "restaurantsList", method: .get, parameters: parametrs, headers: nil, completion: completion)
    }
    
    func deviceTokenUpdate(deviceToken: String, clientId: String, completion: @escaping ((NetworkResponse?, DeviceTokenUpdateResponse?) -> Void)) {
        
        let parametrs = ["deviceToken": deviceToken,
                        "clientId": clientId]
        
        networkService.executeRequest(endpoint: "deviceTokenUpdate", method: .post, parameters: parametrs, headers: nil, completion: completion)
    }
    
    func getMenu(byRestaurantId restaurantId: String, with completion: @escaping ((NetworkResponse?, MenuResponse?) -> Void)) {
        let parametrs = [ "restaurantId" : restaurantId ] as Parameters
        
        networkService.executeRequest(endpoint: "menu", method: .get, parameters: parametrs, headers: nil, completion: completion)
    }
    
    func getMastersList(byRestaurantId restaurantId: String?, dueDate: Double?, with completion: @escaping ((NetworkResponse?, MastersListResponse?) -> Void)){
        var parametrs: [String: Any] = [:]
        
        if let restaurantId = restaurantId {
            parametrs.updateValue(restaurantId, forKey: "restaurantId")
        }
        
        if let dueDate = dueDate {
             parametrs.updateValue(dueDate, forKey: "dueDate")
        }
        
        networkService.executeRequest(endpoint: "mastersList", method: .get, parameters: parametrs, headers: nil, completion: completion)
    }
    
    func makeOrder(_ order: OrderRequest, with completion: @escaping ((NetworkResponse?, OrderResponse?) -> Void)) {
        networkService.executeRequest(endpoint: "makeOrder", method: .post, parameters: order.dictionary, headers: nil, completion: completion)
    }
    
    func check(code: String, for phone: String, completion: @escaping ((NetworkResponse?, VerificationCodeNetworkResponse?) -> Void)) {
        let parameters = [
            "phoneNumber": phone,
            "confirmationCode": code
        ] as Parameters
        
        networkService.executeRequest(endpoint: "checkConfirmationCode", method: .post, parameters: parameters, headers: nil, completion: completion)
    }
    
    func resendCode(to phone: String, completion: @escaping ((NetworkResponse?, ResendVerificationCodeNetworkResponse?) -> Void)) {
        let parameters = [
            "phoneNumber": phone
        ] as Parameters
        
        networkService.executeRequest(endpoint: "resendConfirmationCode", method: .post, parameters: parameters, headers: nil, completion: completion)
    }
    
}
