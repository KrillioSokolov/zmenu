//
//  HookerManCollectionViewCell.swift
//  Menu
//
//  Created by Kirill Sokolov on 16.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol MasterCollectionViewCellDelegate: class {
    
    func MasterCollectionViewCellDidTapInfoButton(_ cell: MasterCollectionViewCell)
    
}

final class MasterCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var avatarMaskImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!
    
    weak var delegate: MasterCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
    }
    
    @IBAction func info(_ sender: Any) {
        delegate?.MasterCollectionViewCellDidTapInfoButton(self)
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: CollectionViewTransformConstants.scaleFactor, y: CollectionViewTransformConstants.scaleFactor)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion: nil)
            }
        }
    }

}

extension MasterCollectionViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        containerView.layer.cornerRadius = styleguide.cornerRadius
        containerView.layer.borderWidth = 1
        
        ratingImageView.tintColor = styleguide.warningColor
        containerView.backgroundColor = styleguide.bubbleColor
    }
    
}


struct CollectionViewTransformConstants {
    
    static let scaleFactor = CGFloat(1.1)
    
}
