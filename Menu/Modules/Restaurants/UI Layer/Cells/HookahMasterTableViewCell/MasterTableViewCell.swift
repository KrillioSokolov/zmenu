//
//  MasterTableViewCell.swift
//  Menu
//
//  Created by Kirill Sokolov on 27.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class MasterTableViewCell: UITableViewCell, RestaurantListTableViewCell,  NibReusable {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var presentImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet private weak var infoButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var avatarContainerView: UIView!
    @IBOutlet weak var heartImageView: UIImageView!
    
    weak var delegate: RestaurantTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.borderWidth = 1
        avatarContainerView.layer.cornerRadius = presentImageView.frame.height / 2
        containerView.addShadowView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func restaurantInfo(_ sender: Any) {
        delegate.didTapRestaurantInfoButton(on: self)
    }
    
    func set(photos: [String]) {
      
    }
    
    func configurate(with listItem: RestaurantListItem) {
        guard let master = listItem as? Master else { return }
        
        nameLabel.text = master.name
        likeCountLabel.text = String(master.rating) + " ⭐️"
        distanceLabel.isHidden = true
        presentImageView.download(image: master.imageURL ?? "", placeholderImage: UIImage(named: "ava_default"))
        presentImageView.tintColor = .white
        descriptionLabel.text = master.description
        
    }
    
}

extension MasterTableViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        containerView.layer.cornerRadius = styleguide.cornerRadius
        containerView.layer.borderWidth = 1
        containerView.backgroundColor = styleguide.bubbleColor
        heartImageView.tintColor = styleguide.warningColor
    }
    
}
