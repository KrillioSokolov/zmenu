//
//  MixListCollectionViewCell.swift
//  Menu
//
//  Created by Kirill Sokolov on 10.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol MixListCollectionViewCellDelegate: class {
    
    func didFlip(_ cell: MixListCollectionViewCell)
    
}

final class MixListCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var infoButtonBlurView: UIVisualEffectView!
    @IBOutlet weak var nameBlurView: UIVisualEffectView!
    @IBOutlet weak var priceBlurView: UIVisualEffectView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mixImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var likeCountBlurView: UIVisualEffectView!
    @IBOutlet weak var heartImageView: UIImageView!
    
    var styleguide: DesignStyleGuide!
    weak var delegate: MixListCollectionViewCellDelegate?
    
    lazy var descriptionView: MixDescriptionView = {
        let view = MixDescriptionView.instantiateFromNib()
        
        view.frame = contentView.bounds
        containerView.insertSubview(view, belowSubview: infoButton)
        
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.cornerRadius = styleguide.cornerRadius
        
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameBlurView.layer.cornerRadius = 6
        priceBlurView.layer.cornerRadius = 6
        likeCountBlurView.layer.cornerRadius = 6
        
        infoButtonBlurView.layer.cornerRadius = 0.5 * infoButton.bounds.size.width
        
        prepareGestureRecognizers()
    }

    private func prepareGestureRecognizers() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(showInfo(_:)))

        leftSwipe.direction = [.left, .right]
        contentView.addGestureRecognizer(leftSwipe)
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                startHighlighting()
            } else {
                endHighlighting()
            }
        }
    }
    
    private func startHighlighting() {
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
            self.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
            self.blurView.effect = UIBlurEffect(style: .dark)
            self.descriptionView.blurEffect.effect = UIBlurEffect(style: .dark)
        }, completion: nil)
    }
    
    private func endHighlighting() {
        UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseOut, animations: {
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.blurView.effect = nil
            self.descriptionView.blurEffect.effect = nil
        }, completion: nil)
    }

    // MARK: - Actions
    
    @IBAction private func showInfo(_ sender: Any) {
        delegate?.didFlip(self)
        flip()
    }
    
    private func flip() {
        let animateOption: UIView.AnimationOptions = .transitionFlipFromLeft
        
        UIView.transition(with: self.contentView, duration: 0.25, options: [animateOption], animations: {
            self.blurView.effect = UIBlurEffect(style: .dark)
            self.changeDescriptionViewHiddenStatus(!self.descriptionView.isHidden)
            self.startHighlighting()
        }, completion:  { _ in
            self.endHighlighting()
        })

    }
    
    func changeDescriptionViewHiddenStatus(_ isHidden: Bool) {
        descriptionView.isHidden = isHidden
        
        if isHidden == true {
            contentView.removeGestureRecognizer(descriptionView.scrollView.panGestureRecognizer)
        } else {
            contentView.addGestureRecognizer(descriptionView.scrollView.panGestureRecognizer)
        }
    }

}

//MARK: - UIStyleGuideRefreshing
extension MixListCollectionViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        containerView.layer.cornerRadius = styleguide.cornerRadius
        nameLabel.textColor = styleguide.labelTextColor
        priceLabel.textColor = styleguide.labelTextColor
        heartImageView.tintColor = styleguide.badgeColor
    }
    
}
