//
//  OrderItemTableViewCell.swift
//  Menu
//
//  Created by Kirill Sokolov on 12.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol OrderItemTableViewCellDelegate: class {
    
    func orderItemTableViewCellDidTapRemoveButton(_ orderItemTableViewCell: OrderItemTableViewCell)
    
}

final class OrderItemTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var orderImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var leadingImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceLabelTrailingConstraint: NSLayoutConstraint!
    
    weak var delegate: OrderItemTableViewCellDelegate? {
        didSet {
            if delegate == nil {
                removeButton.isHidden = true
                priceLabelTrailingConstraint.constant = 10
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 3
    }
    
    @IBAction func remove(_ sender: Any) {
        delegate?.orderItemTableViewCellDidTapRemoveButton(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension OrderItemTableViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        removeButton.tintColor = styleguide.primaryColor
    }
    
}

