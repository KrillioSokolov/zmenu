//
//  RestaurantListTableViewCell.swift
//  Menu
//
//  Created by Stas Chelak on 2/5/19.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol RestaurantListTableViewCell: UIStyleGuideRefreshing {
    
    var delegate: RestaurantTableViewCellDelegate! { get set }
    
    func configurate(with listTtem: RestaurantListItem)
    
}
