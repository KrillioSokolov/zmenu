//
//  RestaurantTableViewCell.swift
//  Menu
//
//  Created by Kirill Sokolov on 28.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol RestaurantTableViewCellDelegate: class {
    
    func didTapRestaurantInfoButton(on cell: UITableViewCell)
    func didSelectRestautant(cell: UITableViewCell)
}

final class RestaurantTableViewCell: UITableViewCell, RestaurantListTableViewCell, NibReusable {
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var infoButtonBlurView: UIVisualEffectView!
    @IBOutlet weak var distanceBlurView: UIVisualEffectView!
    @IBOutlet weak var ratingCountBlurView: UIVisualEffectView!
    @IBOutlet weak var nameBlurView: UIVisualEffectView!
    @IBOutlet weak var heartImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet private weak var infoButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    weak var delegate: RestaurantTableViewCellDelegate!
    weak var styleguide: DesignStyleGuide!
    private var photos: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.layer.borderWidth = 1
        photosCollectionView.registerReusableCell(cellType: RestaurantImageCollectionViewCell.self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        photosCollectionView.contentOffset.x = 0
        pageControl.currentPage = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func restaurantInfo(_ sender: Any) {
        delegate.didTapRestaurantInfoButton(on: self)
    }
    
    func set(photos: [String]) {
        self.photos = photos
        pageControl.isHidden = photos.count < 2
        pageControl.numberOfPages = photos.count
        photosCollectionView.reloadData()
    }
    
    func configurate(with listItem: RestaurantListItem) {
        guard let restaurant = listItem as? Restaurant else { return }
        
        self.nameLabel.text = restaurant.name
        self.likeCountLabel.text = String(restaurant.rating) + " ⭐️"
        self.distanceLabel.isHidden = true
        self.set(photos: restaurant.photos)
    }

}

extension RestaurantTableViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        containerView.layer.cornerRadius = styleguide.cornerRadius
        nameLabel.textColor = styleguide.labelTextColor
        distanceLabel.textColor = styleguide.labelTextColor
        likeCountLabel.textColor = styleguide.labelTextColor
        photosCollectionView.layer.cornerRadius = styleguide.cornerRadius
        containerView.layer.cornerRadius = styleguide.cornerRadius
        
        infoButtonBlurView.layer.cornerRadius = 0.5 * infoButton.bounds.size.width
        containerView.layer.borderWidth = 1
        nameBlurView.layer.cornerRadius = 6
        distanceBlurView.layer.cornerRadius = 6
        ratingCountBlurView.layer.cornerRadius = 6
        heartImageView.tintColor = styleguide.warningColor
    }
    
}

//MARK: - UIScrollViewDelegate
extension RestaurantTableViewCell: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.x
        
        let pageWidth = photosCollectionView.bounds.width
        
        pageControl.currentPage = Int((currentOffset + pageWidth / 2) / pageWidth)
    }
    
}

//MARK: - UICollectionViewDataSource
extension RestaurantTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count > 0 ? photos.count : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: RestaurantImageCollectionViewCell.self)
        
        let defaultImage = UIImage(named: "rest_default")
        
        if photos.count == 0 {
            cell.imageView.image = defaultImage
        } else {
            cell.imageView.download(image: photos[indexPath.item], placeholderImage: defaultImage, showLoader: true)
        }
        
        return cell
    }
    
}

//MARK: - UICollectionViewDelegateFlowLayout
extension RestaurantTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return containerView.bounds.size
    }
    
}

//MARK: - UICollectionViewDelegate
extension RestaurantTableViewCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.didSelectRestautant(cell: self)
    }
    
}
