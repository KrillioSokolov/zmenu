//
//  RestaurantImageCollectionViewCell.swift
//  Menu
//
//  Created by Stas Chelak on 12/13/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class RestaurantImageCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
