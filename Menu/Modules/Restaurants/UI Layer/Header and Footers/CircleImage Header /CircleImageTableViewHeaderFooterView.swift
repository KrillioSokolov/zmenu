//
//  CircleImageTableViewHeaderFooterView.swift
//  Menu
//
//  Created by Kirill Sokolov on 01.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

class CircleImageTableViewHeaderFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var backgroundColorView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
