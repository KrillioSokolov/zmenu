//
//  MastersCollectionViewService.swift
//  Menu
//
//  Created by Kirill Sokolov on 01.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol MastersServiceDelegate: class {
    
    func serviceDidChoseMaster(_ service: MastersCollectionViewService, chosenMaster Master: Master)
    func service(_ service: MastersCollectionViewService, requestInfoOfMaster Master: Master)
    
}

final class MastersCollectionViewService: NSObject  {
    
    var selectedMaster: Master?
    var masters: [Master] = []
    
    private weak var mastersCollectionView: UICollectionView?
    private weak var delegate: MastersServiceDelegate?
    private var styleguide: DesignStyleGuide
    
    init(collectionView: UICollectionView, styleguide: DesignStyleGuide) {
        mastersCollectionView = collectionView
        
        self.styleguide = styleguide
    }
    
    func configurate(with delegate: MastersServiceDelegate) {
        mastersCollectionView?.delegate = self
        mastersCollectionView?.dataSource = self
        mastersCollectionView?.allowsSelection = true
        mastersCollectionView?.registerReusableCell(cellType: MasterCollectionViewCell.self)
        
        self.delegate = delegate
    }
    
    func updateMasters(Masters: [Master]) {
        self.masters = Masters
        
        mastersCollectionView?.reloadData()
    }
    
}

extension MastersCollectionViewService: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return masters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: MasterCollectionViewCell.self)
        
        cell.refreshUI(withStyleguide: styleguide)
        
        let master = masters[indexPath.row]
        
        cell.avatarImageView.download(image: master.imageURL ?? "", placeholderImage: UIImage(named: "ava_default"))
        cell.avatarImageView.tintColor = .white
        cell.nameLabel.text = master.name
        cell.likeCount.text = master.rating + " ⭐️"
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedMaster = masters[indexPath.row]
        
        self.selectedMaster = selectedMaster
        delegate?.serviceDidChoseMaster(self, chosenMaster: selectedMaster)
        
        collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.centeredVertically, animated: true)
    }
    
}

extension MastersCollectionViewService: MasterCollectionViewCellDelegate {
    
    func MasterCollectionViewCellDidTapInfoButton(_ cell: MasterCollectionViewCell) {
        guard let indexPath = mastersCollectionView?.indexPath(for: cell) else { return }
        
        let selectedMaster = masters[indexPath.row]
        
        self.selectedMaster = selectedMaster
        delegate?.service(self, requestInfoOfMaster: selectedMaster)
    }
    
}

extension MastersCollectionViewService: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return Constants.cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      
        guard masters.count == 1 else {
            return UIEdgeInsets(top: 10, left: Constants.inset, bottom: 10, right: Constants.inset) }
        
        let totalCellWidth = Constants.cellSize.width * CGFloat(collectionView.numberOfItems(inSection: 0)) - Constants.spaceBetweenCells * CGFloat(masters.count)
        
        let totalSpacingWidth = Constants.spaceBetweenCells * CGFloat(masters.count - 1)
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 10, left: leftInset, bottom: 10, right: rightInset)
    }
    
}

extension MastersCollectionViewService {
    
    struct Constants {
        
        static let cellSize = CGSize(width: (UIScreen.main.bounds.size.width - inset * 2) / 1.1, height: 150)
        static let spaceBetweenCells = CGFloat(10)
        static let inset = CGFloat(10)
        
    }
    
}
