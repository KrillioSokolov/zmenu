//
//  MixListCollectionViewService.swift
//  Menu
//
//  Created by Kirill Sokolov on 10.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol MixListServiceDelegate: class {
    
    func serviceDidChoseMix(_ service: MixListCollectionViewService, chosenMix mix: DisplayableMix)
    
}

final class MixListCollectionViewService: NSObject  {
 
    private weak var delegate: MixListServiceDelegate?
    private var mixes: [DisplayableMix] = []
    private weak var mixListCollectionView: UICollectionView?
    private var styleguide: DesignStyleGuide!
    
    init(collectionView: UICollectionView) {
        mixListCollectionView = collectionView
    }
    
    func configurate(with delegate: MixListServiceDelegate, styleguide: DesignStyleGuide) {
        mixListCollectionView?.delegate = self
        mixListCollectionView?.dataSource = self
        mixListCollectionView?.registerReusableCell(cellType: MixListCollectionViewCell.self)
        
        self.delegate = delegate
        self.styleguide = styleguide
    }
    
    func updateMixes(with newMixes: [DisplayableMix]) {
        mixes = newMixes
        mixListCollectionView?.reloadData()
    }
 
}

// MARK: - UICollectionViewDataSource
extension MixListCollectionViewService: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mixes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: MixListCollectionViewCell.self)
        
        let mix = mixes[indexPath.row]
        
        cell.styleguide = styleguide
        cell.delegate = self
        
        cell.nameLabel.text = mix.name
        cell.mixImageView.download(image: mix.imageURL ?? "", placeholderImage: UIImage(named: "z_violet"))
        cell.priceLabel.text = String(mix.price) + " " + MenuViewController.Constants.grn
        
        cell.descriptionView.titleLabel.text = mix.name
        cell.descriptionView.descriptionLabel.attributedText = NSAttributedString.make(from: mix.descriptionStrings, font: styleguide.regularFont(ofSize: 12))
        cell.likeCountLabel.text = String(mix.likes) + " ❤️"
        cell.changeDescriptionViewHiddenStatus(mix.isFlipped)
        cell.refreshUI(withStyleguide: styleguide)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.serviceDidChoseMix(self, chosenMix: mixes[indexPath.row])
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension MixListCollectionViewService: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: UIScreen.main.bounds.size.width / 2 - 10, height: UIScreen.main.bounds.size.height / 3)
        
        return size
    }
    
}

// MARK: - MixListCollectionViewCellDelegate
extension MixListCollectionViewService: MixListCollectionViewCellDelegate {
    
    func didFlip(_ cell: MixListCollectionViewCell) {
        guard let indexPath = mixListCollectionView?.indexPath(for: cell) else { return }
        
        mixes[indexPath.item].isFlipped = !mixes[indexPath.item].isFlipped
    }
    
}
