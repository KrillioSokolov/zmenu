//
//  OrderItemsTableViewService.swift
//  Menu
//
//  Created by Kirill Sokolov on 12.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol OrderItemsServiceDelegate: class {
    
    func orderItemsServiceDidDeleteItem(_ service: OrderItemsTableViewService, deletedItem item: DisplayableMix)
    
}

final class OrderItemsTableViewService: NSObject {
    
    private weak var delegate: OrderItemsServiceDelegate?
    
    private weak var orderItemsTableView: UITableView?
    private var styleguide: DesignStyleGuide
    
    private var cellLeadingImageConstraintConstant: CGFloat = 20
    
    var orderedMixes: [DisplayableMix] = []
    var amount: String?
    
    init(tableView: UITableView, styleguide: DesignStyleGuide) {
        orderItemsTableView = tableView
        self.styleguide = styleguide
    }
    
    func configurate(with delegate: OrderItemsServiceDelegate? = nil) {
        orderItemsTableView?.delegate = self
        orderItemsTableView?.dataSource = self
        orderItemsTableView?.registerReusableCell(cellType: OrderItemTableViewCell.self)
        orderItemsTableView?.registerReusableHeaderFooterView(viewType: OrderItemsTableFooterView.self)
        
        self.delegate = delegate
    }
    
    func addMixToOrder(_ mix: DisplayableMix) {
        orderedMixes.insert(mix, at: 0)
        orderItemsTableView?.reloadData()
    }
    
    func updateOrderedMixes(_ mixes: [DisplayableMix]) {
        orderedMixes = mixes
        orderItemsTableView?.reloadData()
    }
    
    func setCellsConfiguration(leadingImageConstraint: CGFloat = 20) {
        cellLeadingImageConstraintConstant = leadingImageConstraint
    }
    
}

extension OrderItemsTableViewService: UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderedMixes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath, cellType: OrderItemTableViewCell.self)
        
        let mix = orderedMixes[indexPath.row]
        
        cell.itemNameLabel.text = mix.name
        cell.orderImageView.download(image: mix.imageURL ?? "", placeholderImage: UIImage(named: "z_violet"))
        cell.priceLabel.text = String(mix.price) + " " + MenuViewController.Constants.grn
        cell.delegate = delegate == nil ? nil : self
        cell.leadingImageConstraint.constant = cellLeadingImageConstraintConstant
        cell.refreshUI(withStyleguide: styleguide)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellHeight
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let amount = amount, let footerView = tableView.dequeueReusableHeaderFooterView(viewType: OrderItemsTableFooterView.self) {
            footerView.totalLabel.text = "Итого: ".localized() + amount + " " + MenuViewController.Constants.grn
            
            return footerView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return amount == nil ? 0 : Constants.footerHeight
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Удалить".localized()) { (action, indexPath) in
            
            self.removeOrderedMix(by: indexPath)
        }
        
        return [delete]
    }

}

extension OrderItemsTableViewService: OrderItemTableViewCellDelegate {
    
    func removeOrderedMix(by indexPath: IndexPath) {
        let deletedMix = self.orderedMixes[indexPath.row]
        
        self.orderedMixes.remove(at: indexPath.row)
        orderItemsTableView?.deleteRows(at: [indexPath], with: .none)
        
        DispatchQueue.main.async {
            self.delegate?.orderItemsServiceDidDeleteItem(self, deletedItem: deletedMix)
        }
    }
    
    func orderItemTableViewCellDidTapRemoveButton(_ orderItemTableViewCell: OrderItemTableViewCell) {
        guard let indexPath = orderItemsTableView?.indexPath(for: orderItemTableViewCell) else { return }
        
        removeOrderedMix(by: indexPath)
    }
    
}


extension OrderItemsTableViewService {
    
    struct Constants {
        
        static let cellHeight = CGFloat(44)
        static let footerHeight = CGFloat(35)
        
    }
    
}
