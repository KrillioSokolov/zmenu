//
//  UIStoryboard.swift
//  Menu
//
//  Created by Chelak Stas on 12/17/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

fileprivate enum AuthorizationStoryboardControllerId: String {
    
    case login = "LoginViewController"
    case verificationCode = "VerificationCodeViewController"
    case privacy = "PrivacyViewControllerStoryboardId"
    
}

extension UIStoryboard {
    
    static private var authorizationStoryboard: UIStoryboard {
        return UIStoryboard(name: "Authorization", bundle: nil)
    }
    
    struct Authorization {
        
        static var loginViewController: LoginViewController {
            return UIStoryboard.authorizationStoryboard.instantiateViewController(withIdentifier: AuthorizationStoryboardControllerId.login.rawValue) as! LoginViewController
        }
        
        static var verificationCodeViewController: VerificationCodeViewController {
            return UIStoryboard.authorizationStoryboard.instantiateViewController(withIdentifier: AuthorizationStoryboardControllerId.verificationCode.rawValue) as! VerificationCodeViewController
        }
        
        static var privacyViewController: PrivacyViewController {
            return UIStoryboard.authorizationStoryboard.instantiateViewController(withIdentifier: AuthorizationStoryboardControllerId.privacy.rawValue) as! PrivacyViewController
        }
        
    }
    
}
