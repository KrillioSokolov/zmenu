//
//  DatePickerViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 17.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

fileprivate struct LocalizedStringConstants {
    
    static var lookTextExample: String { return "Борода и макбук. \nУгловой стол возле барной стойки. \nVIP комната.".localized()
    }
    
    static var suggestionText: String { return "Что бы Вас быстрее нашли в заведении, дайте подсказку или номер стола. 🤠  \nНапример: \nДве брюнетки, одна в красном худи.".localized() }
    
}

final class DatePickerViewController: UIViewController {
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var restaurantStore: RestaurantStore! {
        didSet {
            restaurantStore.addDataStateListener(self)
        }
    }
    
    @IBOutlet weak var lookLikeLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var globalBlurView: UIVisualEffectView!
    @IBOutlet weak var acceptButtonContainerView: CTAButtonContainerView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cityContainerView: UIView!
    @IBOutlet private weak var datePicker: UIDatePicker!
    @IBOutlet private weak var datePickerHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var dueDateContainerView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var lookTextView: UITextView!
    @IBOutlet weak var lookTextViewContainerView: UIView!
    
    private var onceLayoutSubviews = false
    private var dueDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        acceptButtonContainerView.ctaButton.addTarget(self, action: #selector(accept), for: .touchUpInside)
        
        updateDueDateLabel()
        configurateNavBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        acceptButtonContainerView.ctaButton.setTitle(Constants.acceptButtonTitle, for: .normal)
        
        restaurantStore.resetRequestModel() //fucking workaroung need to change concept
        configurateDatePicker()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            
            refreshUI(withStyleguide: styleguide)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
            self.globalBlurView.effect = UIBlurEffect(style: .dark)
        }, completion: nil)
        UIView.animate(withDuration: 0.5, animations: {
            self.logoTopConstraint.constant = 30
            self.view.layoutIfNeeded()
        })
    }
    
    private func configurateNavBar() {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        navigationController?.tabBarController?.tabBar.isHidden = false
        
        navigationItem.setTitleView(withTitle: "Днепр".localized(),
                                    subtitle: "Выберите день и время".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    private func updateDate() {
        dueDate = nil
        datePicker.minimumDate = Date()
        datePicker.setDate(Date(), animated: false)
        updateDueDateLabel()
    }
    
    private func configurateDatePicker() {
        datePicker.minimumDate = Date().adding(minutes: 15)
        datePicker.maximumDate = Date().adding(hours: 24)
        
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
    }
    
    func updateDueDateLabel() {
        let formatter = DateFormatter()
        
        formatter.timeStyle = .short
    
        let dueDateString: String
        
        if dueDate == nil {
            dueDateString = "Cейчас".localized()
        } else {
            let dayString = Date.isDateInToday(from: datePicker.date) ? "Сегодня".localized() : "Завтра".localized()
            dueDateString = dayString + " " + formatter.string(from: datePicker.date)
        }

        let attributeTitle: NSMutableAttributedString =  NSMutableAttributedString(string: dueDateString)
        
        attributeTitle.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(0, attributeTitle.length))
        attributeTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: styleguide.primaryColor, range: NSMakeRange(0, attributeTitle.length))
    }
    
}

extension DatePickerViewController {
    
    @objc func accept() {
        guard segmentedControl.selectedSegmentIndex != -1 else {
            restaurantStore.showErrorMessage("Выберите когда Вы хотите сделать заказ".localized())
            return
        }
        let placeText = lookTextView.text == LocalizedStringConstants.lookTextExample ? nil : lookTextView.text
        
        segmentedControl.selectedSegmentIndex == 0 ? restaurantStore.addPlaceDescription(placeText) : restaurantStore.didSelectDueDate(with: dueDate)
        
        restaurantStore.getRestaurantsList()
    }
    
    @IBAction func dateChanged(_ datePicker: UIDatePicker) {
        dueDate = datePicker.date
        updateDueDateLabel()
    }
    
    @IBAction func changeSegment(_ sender: UISegmentedControl) {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
           
            sender.selectedSegmentIndex == 0 ? self.showLookTextView() : self.showDatePicker()
            
            if self.datePickerHeightConstraint.constant == 10 {
                self.datePickerHeightConstraint.constant = 150
            }
            
            self.view.layoutIfNeeded()
        }, completion: { isCompleted in
            self.scrollView.scrollToBottom(animated: true)
        })
        
    }
    
    private func showDatePicker() {
        datePicker.alpha = 1
        lookTextViewContainerView.alpha = 0
        lookTextView.text = LocalizedStringConstants.lookTextExample
        lookTextView.textColor = styleguide.senderTextColor
        configurateDatePicker()
        dueDate = datePicker.minimumDate ?? Date()
        view.endEditing(false)
    }
    
    private func showLookTextView() {
        datePicker.alpha = 0
        lookTextViewContainerView.alpha = 1
        dueDate = nil
    }
    
    func startCTAButtonLoadingAnimation() {
        acceptButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        acceptButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }

}

extension DatePickerViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        lookTextView.text = LocalizedStringConstants.lookTextExample
        lookTextView.textColor = styleguide.senderTextColor
        lookLikeLabel.text = LocalizedStringConstants.suggestionText
        
        dueDateContainerView.layer.cornerRadius = styleguide.cornerRadius
        dueDateContainerView.layer.borderWidth = 1
        
        cityContainerView.layer.cornerRadius = styleguide.cornerRadius
        cityContainerView.layer.borderWidth = 1
    
        refreshCTAButtonContainerView(acceptButtonContainerView, styleguide: styleguide)
    }
    
}

extension DatePickerViewController: UITextViewDelegate {
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == styleguide.senderTextColor {
            textView.text = ""
            textView.textColor = UIColor.white
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            
            textView.text = LocalizedStringConstants.lookTextExample
            textView.textColor = styleguide.senderTextColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            lookTextView.resignFirstResponder()
        }
        return true
    }
    
}

extension DatePickerViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? RestaurantStoreStateChange {
            restaurantStoreStateChange(change: change)
        }
    }
    
    private func restaurantStoreStateChange(change: RestaurantStoreStateChange) {
        if change.contains(.isLoadingState) {
            restaurantStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation()
        }
        
        if change.contains(.restaurants) {
            guard restaurantStore.restaurants != nil, !(UIApplication.topViewController() is RestaurantsListViewController) else { return }
            
            let value = RestaurantsEvent.NavigationEvent.DidSelectDueDate.Value(animated: true)
            
            dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.DidSelectDueDate.self, result: Result(value: value))
        }
        
    }
    
}

extension DatePickerViewController {
    
    struct Constants {
        
        static let dataPickerHeight = CGFloat(140.0)
        static let acceptButtonTitle = "Подтвердить".localized()
        
    }
    
}

