//
//  LoginViewController.swift
//  Menu
//
//  Created by Chelak Stas on 12/17/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

private enum LoginViewControllerStage {
    case phone
    case name
}

final class LoginViewController: UIViewController {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var phoneTextField: NumberTextField!
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var countryCodeLabel: UILabel!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var nameView: UIView!
    @IBOutlet private weak var plusLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var privacyView: UIView!
    @IBOutlet private weak var privacyTextView: UITextView!
    @IBOutlet private weak var phoneViewBottomConstraint: NSLayoutConstraint!
    
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var restaurantStore: RestaurantStore! {
        didSet {
            restaurantStore.addDataStateListener(self)
        }
    }
    private var currentStage = LoginViewControllerStage.phone {
        didSet {
            didChangeEnterDataStage()
        }
    }
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateUI()
        signForNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        didChangeEnterDataStage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
    
    private func configurateUI() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        view.backgroundColor = styleguide.backgroundScreenColor
        nextButton.tintColor = .white
        nextButton.setPerfectCorners()
        nextButton.setBorder(.white, width: 1)
        
        countryCodeLabel.textColor = styleguide.primaryTextColor
        descriptionLabel.textColor = styleguide.secondaryTextColor
        
        plusLabel.textColor = styleguide.primaryTextColor
        phoneTextField.tintColor = styleguide.primaryColor
        phoneTextField.textColor = styleguide.primaryTextColor
        phoneTextField.setValue(styleguide.hintTextColor, forKeyPath: "_placeholderLabel.textColor")
        
        nameTextField.textColor = styleguide.primaryTextColor
        nameTextField.tintColor = styleguide.primaryColor
        nameTextField.setValue(styleguide.hintTextColor, forKeyPath: "_placeholderLabel.textColor")
        
        titleLabel.text = "Добро пожаловать".localized()
        titleLabel.textColor = styleguide.primaryTextColor
        

        let privacyText = "политикой конфиденциальности".localized()
        let text = "Нажимая \"Далее\" Вы соглашаетесь с нашей".localized() + " " + privacyText
        
        let attributedString = NSMutableAttributedString(string: text)
        
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: styleguide.primaryTextColor,
                                        NSAttributedString.Key.font: styleguide.regularFont(ofSize: 14)],
                                       range: attributedString.mutableString.range(of: text))
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: styleguide.accentTextColor,
                                        NSAttributedString.Key.font: styleguide.regularFont(ofSize: 14)],
                                       range: attributedString.mutableString.range(of: privacyText))
        
        privacyTextView.attributedText = attributedString
    }
    
    // MARK: - Actions
    
    private func didChangeEnterDataStage() {
        switch currentStage {
        case .name:
            nameChanged(nameTextField)
            nameView.isHidden = false
            privacyView.isHidden = false
            nameTextField.becomeFirstResponder()
            descriptionLabel.text = "Уточните как Вас величать.".localized()
            
        case .phone:
            phoneChanged(phoneTextField)
            nameView.isHidden = true
            privacyView.isHidden = true
            phoneTextField.becomeFirstResponder()
            descriptionLabel.text = "Для создания заказа укажите свой действующий номер телефона.".localized()
            
        }

        UIView.animate(withDuration: 0.35, animations: {
            self.view.layoutSubviews()
        })
    }
    
    @objc private func back() {
        let value = RestaurantsEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    @IBAction private func next(_ sender: Any) {
        switch currentStage {
        case .phone:
            currentStage = .name
            
        case .name:
            let correctPhone = (countryCodeLabel.text ?? "") + phoneTextField.cleanNumber
            restaurantStore.newClientMakeOrder(phone: correctPhone, name: nameTextField.text ?? "")
        }
    }

    @IBAction func phoneChanged(_ sender: Any) {
        nextButton.isEnabled = phoneTextField.isValid
    }
    
    @IBAction func nameChanged(_ sender: Any) {
        nextButton.isEnabled = (nameTextField.text?.count ?? 0) > 2
    }
    
    @IBAction func showPrivacy(_ sender: Any) {
        let value = RestaurantsEvent.NavigationEvent.ShowPrivacyController.Value(animated: true)
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.ShowPrivacyController.self, result: Result(value: value))
    }

}

extension LoginViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == phoneTextField, currentStage == .name { currentStage = .phone }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == phoneTextField) {
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = newString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
        }
        else {
            return true
        }
    }

}

//MARK: - Notifications
extension LoginViewController {
    
    private func signForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
        
        let duration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        
        if #available(iOS 11.0, *) {
            phoneViewBottomConstraint.constant = keyboardHeight - additionalSafeAreaInsets.bottom
        } else {
            // Fallback on earlier versions
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        phoneViewBottomConstraint.constant = 0
    }
    
}

//MARK: - DataStateListening
extension LoginViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? RestaurantStoreStateChange {
            authorizationStoreStateChange(change: change)
        }
    }
    
    private func authorizationStoreStateChange(change: RestaurantStoreStateChange) {
        if change.contains(.isLoadingState) {
            activityIndicator.isHidden = !restaurantStore.isLoading
            nextButton.isHidden = restaurantStore.isLoading
        }

        if change.contains(.otp) {
            let value = RestaurantsEvent.NavigationEvent.ShowCodeVerificationController.Value(animated: true)
            self.dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.ShowCodeVerificationController.self, result: Result(value: value))
        }
    }
    
}
