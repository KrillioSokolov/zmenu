//
//  MasterInfoViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 18.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import UIKit

final class MasterInfoViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addressButton: UIButton!
    
    @IBOutlet weak var descriptionUnderlineView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var averageRatingLabel: UILabel!
    @IBOutlet weak var totalsLabel: UILabel!
    
    @IBOutlet weak var instagramTextView: UITextView!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var master: Master!
    var restaurantStore: RestaurantStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurateNavBar()
        refreshUI(withStyleguide: styleguide)
        configurateUI()
    }
    
    var primaryContactFullAddress: String? {
        return master.restaurant.address.country + ", " + master.restaurant.address.city + ", " + master.restaurant.address.street  + " " + master.restaurant.address.house
    }
    
    private func configurateNavBar() {
        navigationItem.addCloseButton(with: self, action: #selector(close), tintColor: styleguide.primaryColor)
        navigationController?.navigationBar.barStyle = .blackOpaque
        navigationItem.setTitleView(withTitle: "Детальная информация".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
    }
    
    
    @objc func close() {
        let value = RestaurantsEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }

    
    @IBAction func showLocaionOnMaps(_ sender: Any) {
        let testURL: NSURL = NSURL(string: "comgooglemaps-x-callback://")!
        if UIApplication.shared.canOpenURL(testURL as URL) {
            if let address = primaryContactFullAddress?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                let directionsRequest: String = "comgooglemaps-x-callback://" + "?daddr=\(address)" + "&x-success=sourceapp://?resume=true&x-source=AirApp"
                let directionsURL: NSURL = NSURL(string: directionsRequest)!
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(directionsURL as URL)) {
                    if #available(iOS 10.0, *) {
                        application.open(directionsURL as URL, options: [:], completionHandler: nil)
                    } else {
                        restaurantStore.showErrorMessage("Извините, у вас устаревшая версия iOS, попробуйте загуглить сами 😢".localized())
                    }
                }
            }
        } else {
            NSLog("Can't use comgooglemaps-x-callback:// on this device.")
        }
    }
    
    deinit {
        print("deinit MasterInfoViewController")
    }
    
    func configurateUI() {
        nameLabel.text = master.name
        ratingLabel.text = "Рейтинг:" + " " + master.rating + " ⭐️"
        averageRatingLabel.text = "Средняя оценка:".localized() + " " + master.averageRate + " ⭐️"
        totalsLabel.text = "Забито чаш:".localized() + " " + master.ordersCount
        descriptionUnderlineView.isHidden = master.description?.isEmpty ?? true
        
        let attributes = [ kCTUnderlineStyleAttributeName : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor.rawValue: UIColor.white] as? [NSAttributedString.Key : Any]
        
        var addressString = master.restaurant.address.street + " " + master.restaurant.address.house
        
        if let floor = master.restaurant.address.floor {
            addressString = addressString + ", " + "этаж ".localized() + floor
        }
        
        if let instagram = master.restaurant.instagram {
            instagramTextView.linkTextAttributes = attributes
            instagramTextView.text = "instagram.com/" + instagram
        }

        avatarImageView.download(image: master.imageURL ?? "", placeholderImage: UIImage(named: "ava_default"))
        
        if let instagram = master.instagram {
            instagramTextView.text = "https://instagram.com/" + instagram
        }
        
        descriptionLabel.text = master.description
        
        addressButton.setAttributedTitle(NSAttributedString(string: addressString, attributes: attributes), for: .normal)
    }

}

extension MasterInfoViewController: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
    }
    
}
