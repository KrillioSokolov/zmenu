//
//  MenuViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 30.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class MenuViewController: UIViewController {
    
    @IBOutlet weak var buckerDividerLineView: UIView!
    @IBOutlet weak var orderButtonBackgroundView: UIView!
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var mixListCollectionView: UICollectionView!
    @IBOutlet private weak var categoryCollectionView: UICollectionView!
    @IBOutlet private weak var orderButtonContainerView: CTAButtonContainerView!
    @IBOutlet private weak var bucketContainerView: UIView!
    @IBOutlet private weak var orderItemsTableView: UITableView!
    @IBOutlet private weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    fileprivate var onceLayoutSubviews = false
    
    fileprivate var categoryCollectionViewService: CategoryCollectionViewService!
    fileprivate var mixListCollectionViewService: MixListCollectionViewService!
    fileprivate var orderItemsTableViewService: OrderItemsTableViewService!
    
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var restaurantStore: RestaurantStore! {
        didSet {
            restaurantStore.addDataStateListener(self)
        }
    }
    
    var restaurantListItem: RestaurantListItem!
    var menu : [DisplayableCategory: [DisplayableMix]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        navigationController?.tabBarController?.tabBar.isHidden = false
        
        navigationItem.setTitleView(withTitle: "Забивает ".localized() + restaurantListItem.name,
                                    subtitle: "Выберите микс".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            
            configurateOrderItemsTableView()
            configurateMixListCollectionView()
            configurateMixCategoryCollectionView()
            configurateCTAButton()
            configurateDisplayableHookaMenu()
            refreshUI(withStyleguide: styleguide)
        }
    }

    deinit {
        print("deinit RestaurantViewController")
    }
    
    @objc func back() {
        let value = RestaurantsEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }

    @objc private func order() {
        let ids = orderItemsTableViewService.orderedMixes.map({ ($0.mixId, $0.categoryId) })
        
        restaurantStore.updateOrderedMixes(with: ids)
        
        restaurantListItem is Master ? openOrderInfo() : restaurantStore.getMastersList(restaurantId: restaurantListItem.restaurantId)
    }
    
    private func openOrderInfo() {
        let value = RestaurantsEvent.NavigationEvent.DidChooseMixesForOrder.Value.init(restaurant: restaurantListItem, mixesForOrder: orderItemsTableViewService.orderedMixes)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.DidChooseMixesForOrder.self, result: Result(value: value))
    }
    
}

extension MenuViewController {
    
    func configurateDisplayableHookaMenu() {
        guard let categories = restaurantStore.menuData?.categories, categories.count > 0 else { return }
        
        menu = categories.reduce([DisplayableCategory: [DisplayableMix]](), { (dict, mixCategory) -> [DisplayableCategory : [DisplayableMix]] in
            var dict = dict
            
            dict[DisplayableCategory(categoryId: mixCategory.categoryId, name: mixCategory.name, imageURL: mixCategory.imageURL)] = mixCategory.mixes.map{DisplayableMix(from: $0)}
            
            return dict
        })
        
        categoryCollectionViewService.updateCategories(categories: categories.map{DisplayableCategory(categoryId: $0.categoryId, name: $0.name, imageURL: $0.imageURL)})
        
        mixListCollectionViewService.updateMixes(with: categories.first?.mixes.compactMap({ DisplayableMix(from: $0) }) ?? [])
    }
    
    func configurateMixCategoryCollectionView() {
        categoryCollectionViewService = CategoryCollectionViewService(colletionView: categoryCollectionView)
        categoryCollectionViewService.configurate(with: self)
    }
    
    func configurateMixListCollectionView() {
        mixListCollectionViewService = MixListCollectionViewService(collectionView: mixListCollectionView)
        mixListCollectionViewService.configurate(with: self, styleguide: styleguide)
    }
    
    func configurateOrderItemsTableView() {
        orderItemsTableViewService = OrderItemsTableViewService(tableView: orderItemsTableView, styleguide: styleguide)
        orderItemsTableViewService.configurate(with: self)
        orderItemsTableView.backgroundColor = styleguide.bubbleColor
        orderItemsTableView.tableFooterView = UIView()
        bucketContainerView.layer.cornerRadius = styleguide.cornerRadius
        bucketContainerView.backgroundColor = styleguide.bubbleColor
    }
    
}

extension MenuViewController: CTAButtonConfiguring, UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshCTAButtonContainerView(orderButtonContainerView, styleguide: styleguide)
    }
    
    func configurateCTAButton() {
        orderButtonContainerView.ctaButton.setTitle("Выберите микс".localized(), for: .disabled)
        orderButtonContainerView.ctaButton.setTitle("Далее".localized(), for: .normal)
        orderButtonContainerView.ctaButton.isEnabled = false
        orderButtonBackgroundView.backgroundColor = styleguide.backgroundScreenColor
        orderButtonContainerView.ctaButton.addTarget(self, action: #selector(order), for: .touchUpInside)
    }
    
    func startCTAButtonLoadingAnimation() {
        orderButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        orderButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
}

extension MenuViewController: CategoryServiceDelegate {
    
    func serviceDidChoseCategory(_ service: CategoryCollectionViewService, chosenCategory category: DisplayableCategory) {
        guard let mixes = menu[category] else { return }
        
        categoryCollectionView.performBatchUpdates(nil, completion: nil)
        mixListCollectionViewService.updateMixes(with: mixes)
        
        mixListCollectionView.reloadData()
    }
    
}

extension MenuViewController: MixListServiceDelegate {
    
    func serviceDidChoseMix(_ service: MixListCollectionViewService, chosenMix mix: DisplayableMix) {
        orderItemsTableViewService.addMixToOrder(mix)
        orderButtonContainerView.ctaButton.isEnabled = true
        buckerDividerLineView.isHidden = false
        shadowView.layer.shadowOpacity = 0.8
        
        UIView.animate(withDuration: 0.3) {
            self.tableViewHeightConstraint.constant = Constants.orderCellHeight * self.tableViewHeightConstraintIndex() - 1
            
            self.view.layoutIfNeeded()
        }
        
        orderItemsTableView.reloadData()
        orderItemsTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
}

extension MenuViewController: OrderItemsServiceDelegate {
    
    func orderItemsServiceDidDeleteItem(_ service: OrderItemsTableViewService, deletedItem item: DisplayableMix) {
        if orderItemsTableViewService.orderedMixes.count == 0 {
            UIView.animate(withDuration: 0.5) {
                self.tableViewHeightConstraint.constant = 0
                self.orderButtonContainerView.ctaButton.isEnabled = false
                self.buckerDividerLineView.isHidden = true
                self.shadowView.layer.shadowOpacity = 0
                self.view.layoutIfNeeded()
            }
        } else if orderItemsTableViewService.orderedMixes.count <= 3 {
            UIView.animate(withDuration: 0.5) {
                self.tableViewHeightConstraint.constant = Constants.orderCellHeight * self.tableViewHeightConstraintIndex()
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    func tableViewHeightConstraintIndex() -> CGFloat {
        let index: CGFloat
        
        switch self.orderItemsTableViewService.orderedMixes.count {
        case 0, 1, 2:
            index = CGFloat(self.orderItemsTableViewService.orderedMixes.count)
        default:
            index = 2.5
        }
        
        return index
    }
    
}

extension MenuViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? RestaurantStoreStateChange {
            restaurantStoreStateChange(change: change)
        }
    }
    
    private func restaurantStoreStateChange(change: RestaurantStoreStateChange) {
        if change.contains(.isLoadingState) {
            restaurantStore.isLoading ? startCTAButtonLoadingAnimation() : stopCTAButtonLoadingAnimation()
            
            //KS: TODO: Show/hide skeleton
            //restaurantStore.isLoading ? addSkeletonViewController() : hideSkeletonViewController()
        }
        
        if change.contains(.menu) {
           configurateDisplayableHookaMenu()
        }
        
        if change.contains(.mastersForRestaurant) {
            openOrderInfo()
        }

    }
    
}

extension MenuViewController {
    
    struct Constants {
        
        static let grn = "₴"
        static let orderCellHeight = CGFloat(44.0)
        
    }
    
}
