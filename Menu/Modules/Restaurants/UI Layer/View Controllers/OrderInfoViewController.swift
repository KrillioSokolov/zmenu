//
//  OrderInfoViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 13.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

private struct LocalizedStringConstants  {
    
    static var commentTextExample: String { return "Коментарий к заказу. \nНапример: Раскурим сами.".localized() }
    
}

final class OrderInfoViewController: UIViewController {
    
    @IBOutlet private weak var orderButtonContainerView: CTAButtonContainerView!
    
    @IBOutlet private weak var peopleCountView: UIView!
    @IBOutlet private weak var peopleCountLabel: UILabel!
    @IBOutlet private weak var stepper: UIStepper!
    @IBOutlet private weak var peopleCountContainerView: UIView!
    @IBOutlet private weak var commentTextView: UITextView!
    
    @IBOutlet private weak var masterContainerView: UIView!
    @IBOutlet private weak var masterLabel: UILabel!
    @IBOutlet private weak var masterBeLabel: UILabel!
    @IBOutlet private weak var MenuCollectionView: UICollectionView!
    @IBOutlet private weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var orderButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet private weak var yourOrderContainer: UIView!
    @IBOutlet private weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var orderItemsTableView: UITableView!
    
    private var orderItemsTableViewService: OrderItemsTableViewService!
    private var mastersCollectionViewService: MastersCollectionViewService!
    
    private var masters: [Master] = []
    
    var mixesForOrder: [DisplayableMix]!
    var restaurantListItem: RestaurantListItem!
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var restaurantStore: RestaurantStore! {
        didSet {
            restaurantStore.addDataStateListener(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateCollectionView()
        configurateTableView()
        configurateNavBar()
        configurateCTAButton()
        handleMasters()
        setupOutlets()
        refreshUI(withStyleguide: styleguide)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func configurateNavBar() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Забивает ".localized() + restaurantListItem.name,
                                    subtitle: "Заполните информацию о заказе".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    func configurateCTAButton() {
        orderButtonContainerView.ctaButton.setTitle("Оформить заказ".localized(), for: .normal)
        orderButtonContainerView.ctaButton.addTarget(self, action: #selector(makeOrder), for: .touchUpInside)
    }
    
    @objc private func makeOrder() {
        guard
            let peopleCount = peopleCountLabel.text,
            let comment = commentTextView.text?.replacingOccurrences(of: LocalizedStringConstants.commentTextExample, with: "") else { return }
        
        if !restaurantStore.makeOrderIfPossible(with: mastersCollectionViewService.selectedMaster?.masterId, peopleCount: peopleCount, comment: comment) {
            let value = RestaurantsEvent.NavigationEvent.ShowLoginController.Value(animated: true)
            
            dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.ShowLoginController.self, result: Result(value: value))
        }
    }
    
}

extension OrderInfoViewController {
    
    private func handleMasters() {
        
        //KS: TODO: Thinking another way to manage different branches of getting Masters.
        if let Master = restaurantListItem as? Master  {
            self.masters = [Master]
        } else if let Masters = restaurantStore.mastersData?.masters {
            self.masters = Masters
        } else {
            return
        }
    
        mastersCollectionViewService.updateMasters(Masters: masters)
        
        configurateCollectionViewHeight()
        
        DispatchQueue.delay(delay: 0.1) {
            self.selectRandomMaster()
        }
    }
    
    private func updateMasterLabel(name: String?) {
        masterLabel.text = name
    }
    
    private func setupOutlets() {
        commentTextView.text = LocalizedStringConstants.commentTextExample
    
        let time: String
        
        if let orderTime = restaurantStore.orderRequestModel.dueDate {
            let date = Date.init(seconds: Int64(ceil(orderTime)))
            
            time = Date.displayableSectionDate(from: date)
        } else {
            time = "Сейчас".localized()
        }
        
        timeLabel.text = "Заказ на: ".localized() + time
    }
    
    private func selectRandomMaster() {
        if masters.count > 0 {
            let randomMasterIndex = Int(arc4random_uniform(UInt32(mastersCollectionViewService.masters.count)))
        
            MenuCollectionView.selectItem(at: IndexPath(row: randomMasterIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
            
            mastersCollectionViewService.selectedMaster = mastersCollectionViewService.masters[randomMasterIndex]
        }
        
        updateMasterLabel(name: mastersCollectionViewService.selectedMaster?.name ?? "Выберите другое заведение".localized())
    }
    
    @objc private func back() {
        let value = RestaurantsEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
}

extension OrderInfoViewController {
    
    @IBAction func step(_ sender: UIStepper) {
        peopleCountLabel.text = String(Int(sender.value))
    }
    
}

extension OrderInfoViewController {
    
    private func configurateTableView() {
        tableViewHeightConstraint.constant = CGFloat(mixesForOrder.count) * OrderItemsTableViewService.Constants.cellHeight - 1
        
        orderItemsTableView.layer.cornerRadius = styleguide.cornerRadius
        
        orderItemsTableViewService = OrderItemsTableViewService(tableView: orderItemsTableView, styleguide: styleguide)
        orderItemsTableViewService.orderedMixes = mixesForOrder
        orderItemsTableViewService.configurate()
        orderItemsTableView?.isUserInteractionEnabled = false
    }
    
    private func configurateCollectionView() {
        mastersCollectionViewService = MastersCollectionViewService(collectionView: MenuCollectionView, styleguide: styleguide)
        
        mastersCollectionViewService.configurate(with: self)
    }
    
    private func configurateCollectionViewHeight() {
        let height: CGFloat
        
        switch masters.count {
        case 0:
            height = 0
        case 1:
            height = MastersCollectionViewService.Constants.cellSize.height * CollectionViewTransformConstants.scaleFactor
            MenuCollectionView.isScrollEnabled = false
        default:
            height = 1.6 * MastersCollectionViewService.Constants.cellSize.height
        }
        
        UIView.animate(withDuration: 0.2) {
            self.collectionViewHeightConstraint.constant = height
            self.view.layoutIfNeeded()
        }
        
    }
    
    private func handleMaster() {
        if (restaurantStore.dueDate == nil || Date.isDateInToday(from: restaurantStore.dueDate ?? Date())), collectionViewHeightConstraint.constant == 0 {
            UIView.animate(withDuration: 0.2) {
                self.collectionViewHeightConstraint.constant = UIScreen.main.bounds.size.height/3 * CollectionViewTransformConstants.scaleFactor
                self.view.layoutIfNeeded()
            }
            selectRandomMaster()
            masterLabel.text = mastersCollectionViewService.selectedMaster?.name
            masterBeLabel.text = "Вас будет обслуживать:".localized()
            masterBeLabel.textAlignment = .left
            
        } else if !Date.isDateInToday(from: restaurantStore.dueDate ?? Date()) {
            UIView.animate(withDuration: 0.2) {
                self.collectionViewHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
            mastersCollectionViewService.selectedMaster = nil
            masterBeLabel.textAlignment = .center
            masterBeLabel.text = "Извините, на выбранный Вами день расписание персонала не сформировано.".localized()
            masterLabel.text = nil
        }
    }
    
    func startCTAButtonLoadingAnimation() {
        orderButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        orderButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
}

extension OrderInfoViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if commentTextView.textColor == styleguide.secondaryTextColor {
            commentTextView.text = nil
            commentTextView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if commentTextView.text.isEmpty {
            commentTextView.text = LocalizedStringConstants.commentTextExample
            commentTextView.textColor = styleguide.secondaryTextColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            commentTextView.resignFirstResponder()
        }
        return true
    }
    
}

extension OrderInfoViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? RestaurantStoreStateChange {
            restaurantStoreStateChange(change: change)
        }
    }
    
    private func restaurantStoreStateChange(change: RestaurantStoreStateChange) {
        if change.contains(.isLoadingState) {
            restaurantStore.isLoading ? startCTAButtonLoadingAnimation() : stopCTAButtonLoadingAnimation()
            
            //KS: TODO: Show/hide skeleton
            //restaurantStore.isLoading ? addSkeletonViewController() : hideSkeletonViewController()
        }
        
        if change.contains(.mastersForRestaurant) {
            self.handleMasters()
        }
    }
    
}

extension OrderInfoViewController: MastersServiceDelegate {
    
    func serviceDidChoseMaster(_ service: MastersCollectionViewService, chosenMaster Master: Master) {
        updateMasterLabel(name: Master.name)
    }
    
    func service(_ service: MastersCollectionViewService, requestInfoOfMaster Master: Master) {
        let value = RestaurantsEvent.NavigationEvent.DidTapInfoButtonOnMasterCell.Value(master: Master)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.DidTapInfoButtonOnMasterCell.self, result: Result(value: value, error: nil))
    }
    
}

extension OrderInfoViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        stepper.tintColor = styleguide.primaryColor
        stepper.layer.cornerRadius = styleguide.cornerRadius
        yourOrderContainer.backgroundColor = styleguide.bubbleColor
        masterContainerView.backgroundColor = .clear
        peopleCountContainerView.backgroundColor = styleguide.bubbleColor
        commentTextView.textColor = styleguide.secondaryTextColor
        refreshCTAButtonContainerView(orderButtonContainerView, styleguide: styleguide)
        
        peopleCountContainerView.layer.cornerRadius = styleguide.cornerRadius
        peopleCountContainerView.layer.borderWidth = 1
        
        masterContainerView.layer.cornerRadius = styleguide.cornerRadius
        masterContainerView.layer.borderWidth = 1
        
        yourOrderContainer.layer.cornerRadius = styleguide.cornerRadius
        yourOrderContainer.layer.borderWidth = 1
    }
    
}

extension OrderInfoViewController {
    
    struct Constants {
        
        static let dataPickerHeight = CGFloat(120.0)
        
    }
    
}
