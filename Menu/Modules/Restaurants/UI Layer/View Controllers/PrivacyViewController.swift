//
//  PrivacyViewController.swift
//  Menu
//
//  Created by Chelak Stas on 2/15/19.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import UIKit

final class PrivacyViewController: UIViewController {
    
    @IBOutlet weak var privacyTextView: UITextView!
    
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var restaurantStore: RestaurantStore!
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateNavBar()
        refreshUI(withStyleguide: styleguide)
    }
    
    private func configurateNavBar() {
        navigationItem.addCloseButton(with: self, action: #selector(close), tintColor: styleguide.primaryColor)

        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
    }
    
    // MARK: - Actions
    
    @objc private func close() {
        let value = RestaurantsEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
}

extension PrivacyViewController: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        privacyTextView.textColor = styleguide.primaryTextColor
    }
    
}
