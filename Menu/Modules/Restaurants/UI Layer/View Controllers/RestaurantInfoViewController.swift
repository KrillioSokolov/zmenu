//
//  RestaurantInfoViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 05.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class RestaurantInfoViewController: UIViewController {

    @IBOutlet private weak var letsMakeButtonContainerView: CTAButtonContainerView!
    @IBOutlet private weak var workingTimeButton: UIButton!
    @IBOutlet private weak var restaurantDescription: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var descriptionUnderlineView: UIView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var instagramTextView: UITextView!
    
    var primaryContactFullAddress: String? {
        return restaurant.address.country + ", " + restaurant.address.city + ", " + restaurant.address.street  + " " + restaurant.address.house
    }
    
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var restaurant: Restaurant!
    var restaurantStore: RestaurantStore! {
        didSet {
            restaurantStore.addDataStateListener(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        configurateUI()
        refreshUI(withStyleguide: styleguide)
        
        letsMakeButtonContainerView.ctaButton.addTarget(self, action: #selector(letsMake), for: .touchUpInside)
        letsMakeButtonContainerView.ctaButton.setTitle("Выбрать".localized(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configurateNavBar()
    }
    
    private func configurateNavBar() {
        navigationItem.addCloseButton(with: self, action: #selector(close), tintColor: styleguide.primaryColor)
        navigationController?.navigationBar.barStyle = .blackOpaque
        navigationItem.setTitleView(withTitle: restaurant.name,
                                    subtitle: restaurant.rating + " ⭐️",
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func showLocaionOnMaps(_ sender: Any) {
        let testURL: NSURL = NSURL(string: "comgooglemaps-x-callback://")!
        if UIApplication.shared.canOpenURL(testURL as URL) {
            if let address = primaryContactFullAddress?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                let directionsRequest: String = "comgooglemaps-x-callback://" + "?daddr=\(address)" + "&x-success=sourceapp://?resume=true&x-source=AirApp"
                let directionsURL: NSURL = NSURL(string: directionsRequest)!
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(directionsURL as URL)) {
                    if #available(iOS 10.0, *) {
                        application.open(directionsURL as URL, options: [:], completionHandler: nil)
                    } else {
                        restaurantStore.showErrorMessage("Извините, у вас устаревшая версия iOS, попробуйте загуглить сами 😢".localized())
                    }
                }
            }
        } else {
            NSLog("Can't use comgooglemaps-x-callback:// on this device.")
        }
    }
    
    deinit {
        print("deinit RestaurantViewController")
    }
    
    func configurateUI() {
        let attributes = [ kCTUnderlineStyleAttributeName : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor.rawValue: UIColor.white] as? [NSAttributedString.Key : Any]
        
        var addressString = restaurant.address.street + " " + restaurant.address.house
        
        if let floor = restaurant.address.floor {
            addressString = addressString + ", " + "этаж ".localized() + floor
        }
        
        if let instagram = restaurant.instagram {
            instagramTextView.linkTextAttributes = attributes
            instagramTextView.text = "instagram.com/" + instagram
        }
        
        addressButton.setAttributedTitle(NSAttributedString(string: addressString, attributes: attributes), for: .normal)
        
        pageControl.isHidden = restaurant.photos.count < 2
        photosCollectionView.registerReusableCell(cellType: RestaurantImageCollectionViewCell.self)
        restaurantDescription.text = restaurant.description
        descriptionUnderlineView.isHidden = restaurant.description?.isEmpty ?? true
        
    }
    
    @objc private func letsMake() {
        restaurantStore.getMenu(for: restaurant)
    }
    
    @IBAction func getWorkingTime(_ sender: Any) {
        
    }
    
    @objc func close() {
        let value = RestaurantsEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    func startCTAButtonLoadingAnimation() {
        letsMakeButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        letsMakeButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }

}

extension RestaurantInfoViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? RestaurantStoreStateChange {
            restaurantStoreStateChange(change: change)
        }
    }
    
    private func restaurantStoreStateChange(change: RestaurantStoreStateChange) {
        if change.contains(.isLoadingState) {
            restaurantStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation()
        }
    }
    
}


extension RestaurantInfoViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        photosCollectionView.layer.cornerRadius = styleguide.cornerRadius
        restaurantDescription.textColor = styleguide.labelTextColor
        Price.textColor = styleguide.labelTextColor
        refreshCTAButtonContainerView(letsMakeButtonContainerView, styleguide: styleguide)
    }
    
}

//MARK: - UIScrollViewDelegate
extension RestaurantInfoViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.x
        
        let pageWidth = photosCollectionView.bounds.width
        
        pageControl.currentPage = Int((currentOffset + pageWidth / 2) / pageWidth)
    }
    
}

//MARK: - UICollectionViewDataSource
extension RestaurantInfoViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return restaurant.photos.count > 0 ? restaurant.photos.count : 1

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: RestaurantImageCollectionViewCell.self)
        
        let defaultImage = UIImage(named: "rest_default")
        
        if restaurant.photos.count == 0 {
            cell.imageView.image = defaultImage
        } else {
            cell.imageView.download(image: restaurant.photos[indexPath.item], placeholderImage: defaultImage, showLoader: true)
        }
        
        return cell
    }
    
}

//MARK: - UICollectionViewDelegateFlowLayout
extension RestaurantInfoViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
}
