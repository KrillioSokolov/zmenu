//
//  RestaurantsListViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 25.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class RestaurantsListViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var restaurantStore: RestaurantStore! {
        didSet {
            restaurantStore.addDataStateListener(self)
        }
    }
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        let attributes = [ NSAttributedString.Key.foregroundColor: UIColor.white ]
        
        refreshControl.attributedTitle = NSAttributedString(string: "Обновляем список...".localized(), attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(getRestaurantsListItem), for: .valueChanged)
        
        refreshControl.tintColor = .white
        
        return refreshControl
    }()
    
    var restaurants: [Restaurant] {
        return restaurantStore.restaurants ?? []
    }
    
    var masters: [Master]? {
        return restaurantStore.mastersData?.masters
    }
    
    private var onceLayoutSubviews = false
    private var isRestaurantsList = true
    private var displayableData: [RestaurantListItem] {
        return isRestaurantsList ? restaurants : masters ?? []
    }
    
    private var selectedItem: RestaurantListItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateNavBar()
        configurateTableView()
        refreshUI(withStyleguide: styleguide)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        handleEmptyListItems(restaurants)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
        }
    }
    
    private func configurateNavBar() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        navigationController?.tabBarController?.tabBar.isHidden = false
        
        tabBarController?.tabBar.isHidden = false
        
        navigationItem.setTitleView(withTitle: "Днепр".localized(),
                                    subtitle: "Выберите заведение".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
}

extension RestaurantsListViewController {
    
    @objc private func getRestaurantsListItem() {
        isRestaurantsList ? getResaurantsList() : getMastersList()
    }
    
    private func getResaurantsList() {
        restaurantStore.getRestaurantsList()
    }
    
    private func getMastersList() {
        restaurantStore.getMastersList()
    }
    
    private func handleEmptyListItems(_ items: [RestaurantListItem]) {
        if items.count == 0 {
            tableView?.showEmptyListMessage("Список пуст \nПотяните вниз, что бы обновить 👆", textColor: .white)
        } else {
            tableView?.showEmptyListMessage("", textColor: .white)
        }
    }
    
    private func configurateTableView() {
        tableView.registerReusableCell(cellType: RestaurantTableViewCell.self)
        tableView.registerReusableCell(cellType: MasterTableViewCell.self)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundView = nil
    }
    
}

extension RestaurantsListViewController {
    
    @IBAction func changeSegment(_ sender: Any) {
        isRestaurantsList = !isRestaurantsList
        
        let title = isRestaurantsList ? "Выберите заведение".localized() : "Выберите лучшего".localized()
        
        navigationItem.setTitleView(withTitle: "Днепр".localized(),
                                    subtitle: title,
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        
        isRestaurantsList ? restaurantStore.getRestaurantsList() : restaurantStore.getMastersList()
    }
    
    @objc func back() {
        let value = RestaurantsEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
}

extension RestaurantsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return isRestaurantsList ? restaurants.count : masters?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell & RestaurantListTableViewCell = isRestaurantsList ?
            tableView.dequeueReusableCell(indexPath, cellType: RestaurantTableViewCell.self) :
            tableView.dequeueReusableCell(indexPath, cellType: MasterTableViewCell.self)
        
        let displayableItem = displayableData[indexPath.row]
        
       
        
        cell.configurate(with: displayableItem)
        cell.refreshUI(withStyleguide: styleguide)
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height / 3.5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let restaurantListItem = displayableData[indexPath.row]
        
        selectRestaurantListItem(restaurantListItem, indexPath: indexPath)
    }
    
    private func selectRestaurantListItem(_ restaurantListItem: RestaurantListItem, indexPath: IndexPath) {
        selectedItem = restaurantListItem
        restaurantStore.didChooseRestaurantListItem(restaurantListItem: restaurantListItem)
        
        showSpinner() //KS: TODO: Need to create an alghoritm for correct handling all spinners.
        restaurantStore.getMenu(for: restaurantListItem)
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

extension RestaurantsListViewController: RestaurantTableViewCellDelegate {
    
    func didTapRestaurantInfoButton(on cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        
        let listItem = displayableData[indexPath.row]
        
        selectedItem = listItem
        
        if let restaurant = listItem as? Restaurant {
            let value = RestaurantsEvent.NavigationEvent.DidTapInfoButtonOnRestaurantCell.Value(restaurant: restaurant)
            
            dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.DidTapInfoButtonOnRestaurantCell.self, result: Result(value: value, error: nil))
        } else if let master = listItem as? Master {
            let value = RestaurantsEvent.NavigationEvent.DidTapInfoButtonOnMasterCell.Value(master: master)
            
            dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.DidTapInfoButtonOnMasterCell.self, result: Result(value: value, error: nil))
        }
        
       
    }
    
    func didSelectRestautant(cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        
        let restaurantListItem = displayableData[indexPath.row]
        
        selectRestaurantListItem(restaurantListItem, indexPath: indexPath)
    }
    
}

extension RestaurantsListViewController: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        segmentedControl.tintColor = UIColor.white.withAlphaComponent(0.4)
        
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
    }
    
}

extension RestaurantsListViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? RestaurantStoreStateChange {
            restaurantStoreStateChange(change: change)
        }
    }
    
    private func restaurantStoreStateChange(change: RestaurantStoreStateChange) {
        if change.contains(.isLoadingState) {
            if !restaurantStore.isLoading {
                self.refreshControl.endRefreshing()
                self.hideSpinner()
            } else {
                guard !isSpinerPresented else { return }
                
                self.refreshControl.beginRefreshingManually()
            }
        }
        
        if change.contains(.restaurants) {
            tableView.reloadData()
        }
        
        if change.contains(.bestMasters) {
            self.handleEmptyListItems(masters ?? [])
            
            tableView.reloadData()
        }
        
         if change.contains(.menu) {
            //KS: TODO: Should to be refactored.
            
            let value = RestaurantsEvent.NavigationEvent.DidChooseRestaurant.Value(restaurant: selectedItem)
            
            dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.DidChooseRestaurant.self, result: Result(value: value, error: nil))
        }
       
    }
    
}

