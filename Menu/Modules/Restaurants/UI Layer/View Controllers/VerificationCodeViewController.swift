//
//  VerificationCodeViewController.swift
//  Menu
//
//  Created by Stas Chelak on 12/17/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class VerificationCodeViewController: UIViewController {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var codeTextField: UITextField!
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var resendCodeButton: UIButton!
    @IBOutlet private weak var countdownLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet private weak var contentBottomContraint: NSLayoutConstraint!
    
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var restaurantStore: RestaurantStore! {
        didSet {
            restaurantStore.addDataStateListener(self)
        }
    }
    
    private var countdownTimer: Timer?
    private var timeRemaining: TimeInterval = 0
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateUI()
        configurateResendCodeTimer()
        signForNotifications()
        codeTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
        killTimer()
    }
    
    deinit {
        print("VerificationCodeViewController deinit")
    }
    
    private func configurateUI() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        view.backgroundColor = styleguide.backgroundScreenColor
        nextButton.tintColor = .white
        nextButton.setPerfectCorners()
        nextButton.setBorder(.white, width: 1)
        resendCodeButton.tintColor = styleguide.primaryColor
        resendCodeButton.setTitle("Отправить код снова", for: .normal)
        countdownLabel.textColor = styleguide.disabledTextColor

        if #available(iOS 12.0, *) {
            codeTextField.textContentType = .oneTimeCode
        }
        
        codeTextField.tintColor = styleguide.primaryColor
        codeTextField.textColor = styleguide.primaryTextColor
        codeTextField.setValue(styleguide.hintTextColor, forKeyPath: "_placeholderLabel.textColor")
        
        titleLabel.textColor = styleguide.primaryTextColor
        titleLabel.text = "Подтвердите номер".localized()
        
        descriptionLabel.textColor = styleguide.secondaryTextColor
        descriptionLabel.text = "На Ваш номер +(XX) отправленно сообщение с кодом подтверждения.".localized().replacingOccurrences(of: "(XX)", with: restaurantStore.orderRequestModel.phoneNumber?.formattedPhoneNumber() ?? "")
        
    }
    
    // MARK: - Actions
    
    @IBAction func codeChanged(_ sender: Any) {
        let regex = "^[0-9]{6,6}$"
        let isValid = NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: codeTextField.text ?? "")
        nextButton.isEnabled = isValid
        
        if isValid {
            restaurantStore.checkAuth(code: codeTextField.text ?? "")
        }
    }
    
    @objc private func back() {
        let value = RestaurantsEvent.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: RestaurantsEvent.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    @IBAction private func next(_ sender: Any) {
        restaurantStore.checkAuth(code: codeTextField.text ?? "")
    }
    
    
    @IBAction private func resendCode(_ sender: Any) {
        restaurantStore.resendAuthCode()
    }
    
}

// MARK: - Timer
extension VerificationCodeViewController {
    
    private func configurateResendCodeTimer() {
        timeRemaining = 60
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc private func updateTimer() {
        timeRemaining -= 1
        
        if timeRemaining == 0 {
            killTimer()
        }
        
        resendCodeButton.isEnabled = timeRemaining == 0
        countdownLabel.isHidden = timeRemaining == 0
        countdownLabel.text = "(\(Int(timeRemaining)))"
    }
    
    private func killTimer() {
        countdownTimer?.invalidate()
        countdownTimer = nil
    }
    
}

extension VerificationCodeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        return updatedText.count <= 6
    }
    
}

//MARK: - Notifications
extension VerificationCodeViewController {
    
    private func signForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
        
        contentBottomContraint.constant = keyboardHeight
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        contentBottomContraint.constant = 0
    }
    
}

//MARK: - DataStateListening
extension VerificationCodeViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? RestaurantStoreStateChange {
            authorizationStoreStateChange(change: change)
        }
    }
    
    private func authorizationStoreStateChange(change: RestaurantStoreStateChange) {
        if change.contains(.isLoadingState) {
            activityIndicator.isHidden = !restaurantStore.isLoading
            nextButton.isHidden = restaurantStore.isLoading
        }
    }
    
}
