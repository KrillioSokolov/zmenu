//
//  MixDescriptionView.swift
//  Menu
//
//  Created by Stas Chelak on 11/27/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class MixDescriptionView: UIView {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var glassView: UIView!
    @IBOutlet weak var blurEffect: UIVisualEffectView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.addDefaultSoftShadow()
        descriptionLabel.addDefaultSoftShadow()
        
    }
    
}

extension MixDescriptionView: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        titleLabel.textColor = styleguide.labelTextColor
        descriptionLabel.textColor = styleguide.labelTextColor
    }
    
}
