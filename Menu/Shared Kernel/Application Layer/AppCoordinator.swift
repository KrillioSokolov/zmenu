//
//  AppCoordinator.swift
//  Menu
//
//  Created by Kirill Sokolov on 24.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit
import Alamofire

final class AppCoordinator: Coordinator {

    private(set) unowned var window: UIWindow
    private var root: TabBarController!
    
    // coordinators
    private var tabBarCoordinator: TabBarCoordinator!
    private var restaurantCoordinator: RestaurantsCoordinator!
    private var ordersListCoordinator: OrdersListCoordinator!
    
    private(set) var remoteNotificationHandler: RemoteNotificationHandler?
    private(set) var userNotificationCenterHelper = UserNotificationCenterHelper()
    
    
    private var userSessionService: UserSessionService!
    
    private var enterBackgroundDate: Date?
    private var deviceToken: String?
    
    var dispatcher: Dispatcher {
        return context.dispatcher
    }
    
    init(window: UIWindow) {
        self.window = window
        
        let baseURL = NetworkConstants.devServerBaseURL
        let networkService = HTTPNetworkService(baseURL: baseURL, requestExecutor: NetworkRequestExecutor())
        
        super.init(context: CoordinatingContext(dispatcher: DefaultDispatcher(), styleguide: DarkThemeDesignStyleGuide(), networkService: networkService, userSessionService: UserSessionService()))
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        remoteNotificationHandler = RemoteNotificationHandler(dispatcher: dispatcher)
        
        prepareForStart()
        setupRootViewController()
        
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        enterBackgroundDate = Date()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        if let interval = enterBackgroundDate?.timeIntervalSince1970, Date().timeIntervalSince1970 - interval > 600  {
            restaurantCoordinator.popStackToRoot()
        }
    }
    
    override func prepareForStart() {
        super.prepareForStart()
        
        register()
    }

    private func setupRootViewController() {
        tabBarCoordinator = TabBarCoordinator(context: context)
        tabBarCoordinator.prepareForStart()
        
        restaurantCoordinator = RestaurantsCoordinator(context: context)
        restaurantCoordinator.prepareForStart()
        
        if let deviceToken = deviceToken {
            restaurantCoordinator.deviceTokenUpdate(deviceToken: deviceToken)
        }
        
        ordersListCoordinator = OrdersListCoordinator(context: context)
        ordersListCoordinator.prepareForStart()
        
        tabBarCoordinator.addTabCoordinators(coordinators: [restaurantCoordinator, ordersListCoordinator])
        
        let rootViewController = tabBarCoordinator.createFlow()
        
        root = rootViewController
        window.rootViewController = rootViewController
    }
    
    private func configurateTabBarController() {
        
    }
    
    func deviceTokenUpdate(deviceToken: String) {
        if restaurantCoordinator == nil {
            self.deviceToken = deviceToken
        } else {
            restaurantCoordinator.deviceTokenUpdate(deviceToken: deviceToken)
        }
    }
    
}

extension AppCoordinator {
    
    func register() {
        registerShowingSusscesAlert()
        registerShowingErrorAlert()
        registerOrderInfo()
        registerEndMadeOrder()
    }
    
    func registerShowingSusscesAlert() {
        context.dispatcher.register(type: AlertEvent.ShowSuccessAlert.self) {  result, _ in
            switch result {
            case .success(let box):
                if let topVC = UIApplication.topViewController() {
                    topVC.presentSuccess(message: box.message)
                }
            case .failure(_):
                break
            }
        }
    }
    
    func registerShowingErrorAlert() {
        context.dispatcher.register(type: AlertEvent.ShowErrorAlert.self) { result, _ in
            
            switch result {
            case .success(let box):
                if let topVC = UIApplication.topViewController() {
                    topVC.presentError(message: box.message)
                }
            case .failure(_):
                break
            }
        }
    }
    
    func registerEndMadeOrder() {
        context.dispatcher.register(type: AppEvents.OrderEvents.DidMadeOrder.self) { [weak self] result, _ in
            switch result {
            case .success(_):
                self?.root.selectedViewController = self?.ordersListCoordinator.createFlow()
                self?.restaurantCoordinator.popStackToRoot()
                
            case .failure(_):
                break
            }
        }
    }
    
    func registerOrderInfo() {
        context.dispatcher.register(type: OrdersEvent.NavigationEvent.OrderInfo.self) { [weak self] result, _ in
            switch result {
            case .success(_):
                self?.tabBarCoordinator.selectTab(.orders)
            case .failure(_):
                break
            }
        }
    }
    
}
