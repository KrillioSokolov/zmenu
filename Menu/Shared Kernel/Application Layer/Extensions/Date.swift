//
//  Date.swift
//  Menu
//
//  Created by Kirill Sokolov on 20.12.2018.
//  Copyright © 2018 Menu. All rights reserved.
//

import Foundation

extension Date {
    
    var millisecondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    var secondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970).rounded())
    }
    
    init(milliseconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    init(seconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(seconds))
    }
    
}

extension Date {
    
    private static var calendar = Calendar.current
    private static var dateFormatter = DateFormatter()
    
    static var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
    
    static func fromNow(days: Int) -> Date {
        return Date().addingTimeInterval(TimeInterval(days * 34560))
    }
    
    func adding(hours: Int) -> Date {
        return Date.calendar.date(byAdding: .hour, value: hours, to: self) ?? Date()
    }
    
    func adding(minutes: Int) -> Date {
        return Date.calendar.date(byAdding: .minute, value: minutes, to: self) ?? Date()
    }
    
    func years(from date: Date) -> Int {
        return Date.calendar.dateComponents([.year], from: date, to: self).year ?? 0
    }
    
    func months(from date: Date) -> Int {
        return Date.calendar.dateComponents([.month], from: date, to: self).month ?? 0
    }
    
    func weeks(from date: Date) -> Int {
        return Date.calendar.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    
    func days(from date: Date) -> Int {
        return Date.calendar.dateComponents([.day], from: date, to: self).day ?? 0
    }
    
    func hours(from date: Date) -> Int {
        return Date.calendar.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    func minutes(from date: Date) -> Int {
        return Date.calendar.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func seconds(from date: Date) -> Int {
        return Date.calendar.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    static func isTheSameDay(date1: Date, date2: Date) -> Bool {
        return Date.calendar.compare(date1, to: date2, toGranularity: .day) == .orderedSame
    }
    
    static func isDateInToday(from date: Date) -> Bool {
        return Date.calendar.isDateInToday(date)
    }
    
    static func isDateInYesterday(from date: Date) -> Bool {
        return Date.calendar.isDateInYesterday(date)
    }
    
    func isInSameYear() -> Bool {
        return Date.calendar.isDate(self, equalTo: Date(), toGranularity: .year)
    }
    
}

extension Date {
    
    static func time(from date: Date) -> String {
        let comp = calendar.dateComponents([.hour, .minute], from: date)
        var hour = "00"
        var minute = "00"
        
        if let min = comp.minute {
            minute = min < 10 ? "0\(min)" : "\(min)"
        }
        
        if let hours = comp.hour {
            hour = hours < 10 ? "0\(hours)" : "\(hours)"
        }
        
        return "\(hour):\(minute)"
    }
    
    static func displayableSectionDate(from date: Date) -> String {
        DateFormatter.setApplicationLocale(for: Date.dateFormatter)
        
        if Date.isDateInToday(from: date) {
            Date.dateFormatter.dateFormat = "HH:mm"
            return Date.dateFormatter.string(from: date) + " " + "Сегодня".localized()
        } else if Date.isDateInYesterday(from: date) {
            Date.dateFormatter.dateFormat = "HH:mm"
            return Date.dateFormatter.string(from: date) + " " + "Вчера".localized()
        } else {
            Date.dateFormatter.dateFormat = "HH:mm dd.MM.yy"
            
            return Date.dateFormatter.string(from: date)
        }
    }
    
}

extension DateFormatter {
    
    static func setApplicationLocale(for dateFormatter: DateFormatter) {
        guard let language = MenuDefaults().currentLanguage, dateFormatter.locale.identifier != language else { return }
        
        dateFormatter.locale = Locale(identifier: language)
    }
    
}
