//
//  OptionSetBit.swift
//  Menu
//
//  Created by Kirill Sokolov on 28.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

extension Int {
    
    static let bit1: Int = 1
    static let bit2: Int = 2
    static let bit3: Int = 4
    static let bit4: Int = 8
    static let bit5: Int = 16
    static let bit6: Int = 32
    static let bit7: Int = 64
    static let bit8: Int = 128
    static let bit9: Int = 256
    static let bit10: Int = 512
    static let bit11: Int = 1024
    static let bit12: Int = 2048
    static let bit13: Int = 4096
    static let bit14: Int = 8192
    static let bit15: Int = 16384
    static let bit16: Int = 32768
    static let bit17: Int = 65536
    static let bit18: Int = 131072
}
