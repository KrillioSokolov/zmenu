//
//  UserSaved.swift
//  Menu
//
//  Created by Stas Chelak on 12/18/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

final class Client: NSObject, NSCoding, Codable {
    
    let name: String
    let phone: String
    let clientId: String
    
    init(name: String, phone: String, clientId: String) {
        self.name = name
        self.phone = phone
        self.clientId = clientId
    }
    
    required init(coder decoder: NSCoder) {
        name = decoder.decodeObject(forKey: "name") as? String ?? ""
        phone = decoder.decodeObject(forKey: "phone") as? String ?? ""
        clientId = decoder.decodeObject(forKey: "clientId") as? String ?? ""
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(clientId, forKey: "clientId")
    }

}
