//
//  AlertEvent.swift
//  Menu
//
//  Created by Kirill Sokolov on 26.01.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

struct AlertEvent {
        
    struct ShowSuccessAlert: Event {
        typealias Payload = Value
        
        struct Value {
            let message: String
        }
        
    }
    
    struct ShowErrorAlert: Event {
        typealias Payload = Value
        
        struct Value {
            let message: String
        }
        
    }
    
}

