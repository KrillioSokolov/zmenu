//
//  AppEvents.swift
//  Menu
//
//  Created by Stas Chelak on 2/8/19.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

struct AppEvents {
    
    struct OrderEvents {
        
        struct DidMadeOrder: Event {
            typealias Payload = Value
            
            struct Value {
            }
        }

    }
    
}
