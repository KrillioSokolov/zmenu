//
//  GeneralNetworkErrorPresenting.swift
//  Menu
//
//  Created by Maxim Letushov on 6/6/17.
//  Copyright © 2017 Menu. All rights reserved.
//

import Foundation

protocol GeneralNetworkErrorPresenting {
    
    func presentGeneralNetworkError(_ error: GeneralNetworkPresentingError)
    
}
