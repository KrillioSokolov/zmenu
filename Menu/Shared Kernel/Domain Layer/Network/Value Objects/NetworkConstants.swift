//
//  NetworkConstants.swift
//  Menu
//
//  Created by Kirill Sokolov on 25.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct NetworkConstants {
    
    static var devServerBaseURL: URL {
        return NetworkConstants.prodServerURL
    }
    
    static let kirillNgrokURL = URL(string: "https://889bf10c.ngrok.io/")!
    static let nikaNgrokURL = URL(string: "https://728a2736.ngrok.io/")!
    static let stasNgrokURL = URL(string: "https://23e0b0a3.ngrok.io/")!
    static let localHostUrl = URL(string: "http://localhost:8081/")!
    static let prodServerURL = URL(string:"https://skaix.com/")!
    
}
