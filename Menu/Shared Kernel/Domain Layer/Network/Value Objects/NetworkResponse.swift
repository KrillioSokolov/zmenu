//
//  NetworkResponse.swift
//  Menu
//
//  Created by Kirill Sokolov on 12/14/18.
//  Copyright © 2017 Menu. All rights reserved.
//

import Foundation

enum ResultStatus: String, Decodable {
    
    case ok = "ok"
    case error = "error"

}

final class NetworkResponse: Decodable {
    
    struct ServerError: Decodable {
        let errorType: String?
        let errorCode: String?
        let errorText: String?
    }

    var networkInnerError: NetworkResponseInnerError?
    let httpStatusCode: Int?
    var error: Error?
    
    var result: ResultStatus?
    var requestId: String?
    var action: String?
    var data: Data?
    let serverError: ServerError?

    init(result: ResultStatus?, networkInnerError: NetworkResponseInnerError?, requestId: String?, action: String?, data: Data?, httpStatusCode: Int?, error: Error?) {
        self.result = result
        self.networkInnerError = networkInnerError
        self.requestId = requestId
        self.action = action
        self.data = data
        self.httpStatusCode = httpStatusCode
        self.serverError = nil
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        networkInnerError = nil
        httpStatusCode = nil
        
        result = try container.decodeWrapper(key: .result, defaultValue: nil)
        requestId = try container.decodeWrapper(key: .reqId, defaultValue: nil)
        action = try container.decodeWrapper(key: .action, defaultValue: nil)
        serverError = try container.decodeWrapper(key: .error, defaultValue: nil)
    }
    
    enum CodingKeys: String, CodingKey {
        
        case result, reqId, action, data, httpStatusCode, error
        
    }

}

