//
//  CoordinatingContext.swift
//  Menu
//
//  Created by Sokolov Kirill on 5/2/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import Foundation

final class CoordinatingContext {
    
    let dispatcher: Dispatcher
    var dataBaseService: CoreDataDBService?
    let styleguide: DesignStyleGuide
    let networkService: HTTPNetworkService
    let userSessionService: UserSessionService
    
    
    init(dispatcher: Dispatcher, styleguide: DesignStyleGuide, networkService: HTTPNetworkService, userSessionService: UserSessionService) {
        self.styleguide = styleguide
        self.dispatcher = dispatcher
        self.networkService = networkService
        self.userSessionService = userSessionService
    }
    
}
