//
//  MenuNotificationService.swift
//  Menu
//
//  Created by Kirill Sokolov on 18.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

protocol MenuNotificationServiceDelegate: class {
    
    func channelsNotificationService(_ channelsNotificationService: MenuNotificationService,  wantsToHandleRemoteNotification userInfo: [String: Any])
    
}

final public class MenuNotificationService: NSObject {
    
    private let remoteNotificationHandler: RemoteNotificationHandler
    private weak var channelsNotificationServiceDelegate: MenuNotificationServiceDelegate?
    
    private var networkService: HTTPNetworkService?
    private var userNotificationCenterHelper: UserNotificationCenterHelper?
    
    init(remoteNotificationHandler: RemoteNotificationHandler, channelsNotificationServiceDelegate: MenuNotificationServiceDelegate) {
        self.remoteNotificationHandler = remoteNotificationHandler
        self.channelsNotificationServiceDelegate = channelsNotificationServiceDelegate
    }
    
    func configurate(with networkService: HTTPNetworkService, userNotificationCenterHelper: UserNotificationCenterHelper) {
        self.networkService = networkService
        self.userNotificationCenterHelper = userNotificationCenterHelper
    }
    
    @discardableResult
    @objc public func handleRemoteNotification(_ userInfo: [String : Any]) -> Bool {
        guard MenuNotificationService.isMenuRemoteNotification(userInfo) else { return false }
        
        remoteNotificationHandler.handleRemoteNotification(userInfo)
        
        return true
    }
    
    @discardableResult
    @objc public func handleSilentNotification(_ userInfo: [String : Any], fetchComplitionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void) -> Bool {
        guard MenuNotificationService.isMenuRemoteNotification(userInfo) else { return false }
        
        if isActualAction(NotificationConstants.Keys.presenceCheck, userInfo: userInfo) {
//            guard let data = userInfo[NotificationConstants.Keys.data] as? [String : Any],
//                let ref = data[NotificationConstants.Keys.ref] as? String else {
//                    fetchComplitionHandler(.noData)
//
//                    return true
//            }
            
            //networkService?.presenceStatus(ref: ref)
        } else {
            channelsNotificationServiceDelegate?.channelsNotificationService(self, wantsToHandleRemoteNotification: userInfo)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            fetchComplitionHandler(.newData)
        }
        
        return true
    }
    
    @available(iOS 10.0, *)
    @discardableResult
    @objc public func handlePresentingOptionForNotification(_ notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) -> Bool {
        guard let userInfo = notification.request.content.userInfo as? [String: Any], MenuNotificationService.isMenuRemoteNotification(userInfo) else { return false }
        
        let presentionOptions: UNNotificationPresentationOptions = isActualAction(NotificationConstants.Keys.presenceCheck, userInfo: userInfo) ? [] : [.badge, .sound, .alert]
        
        completionHandler(presentionOptions)
        
        return true
    }
    
    @available(iOS 10.0, *)
    @discardableResult
    @objc public func handleNotificationRequest(_ userInfo: [String: Any], delegate: UNUserNotificationCenterDelegate) -> Bool {
        guard MenuNotificationService.isMenuRemoteNotification(userInfo) else { return false }
        
        userNotificationCenterHelper?.handleNotificationRequest(userInfo, delegate: delegate)
        
        return true
    }
    
    @discardableResult
    @objc public func handleNotificationRequest(_ userInfo: [String: Any]) -> Bool {
        guard MenuNotificationService.isMenuRemoteNotification(userInfo) else { return false }
        
        userNotificationCenterHelper?.handleNotificationRequest(userInfo)
        
        return true
    }
    
    @objc static public func isMenuRemoteNotification(_ userInfo: [String : Any]) -> Bool {
        return (userInfo[NotificationConstants.Keys.action] != nil && userInfo[NotificationConstants.Keys.data] != nil)
    }
    
    private func isActualAction(_ actualAction: String, userInfo: [String : Any]) -> Bool {
        guard let action = userInfo[NotificationConstants.Keys.action] as? String else { return false }
        
        return action == actualAction
    }
    
    
}
