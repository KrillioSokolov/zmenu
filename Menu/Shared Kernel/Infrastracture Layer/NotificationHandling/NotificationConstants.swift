//
//  NotificationConstants.swift
//  Menu
//
//  Created by Kirill Sokolov on 06.03.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

struct NotificationConstants {
    
    struct Identifiers {
        
        static let orderInfo = "orderInfoIdentifier"
        static let rating = "ratingIdentifier"
        
    }
    
    struct Keys {
        
        static let action = "action"
        static let aps = "aps"
        static let alert = "alert"
        static let title = "title"
        static let body = "body"
        static let data = "data"
        static let sound = "sound"
        static let soundValue = "default"
        static let presenceCheck = "presenceCheck"
        
    }
    
}
