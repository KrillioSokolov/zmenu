//
//  NotificationContent.swift
//  Menu
//
//  Created by Kirill Sokolov on 06.03.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UserNotifications

@available(iOS 10.0, *)
final class NotificationContent: UNMutableNotificationContent {
    
    init(body: String, title: String? = nil, subtitle: String? = nil, userInfo: [String: Any]) {
        super.init()
        if let title = title {
            self.title = title
        }
        if let subtitle = subtitle {
            self.subtitle = subtitle
        }
        self.body = body
        self.sound = UNNotificationSound.default
        
        if let subtitle = subtitle {
            self.subtitle = subtitle
        }
        
        self.userInfo = userInfo
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
