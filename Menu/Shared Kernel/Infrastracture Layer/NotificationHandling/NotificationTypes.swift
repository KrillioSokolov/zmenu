//
//  NotificationTypes.swift
//  Menu
//
//  Created by Kirill Sokolov on 06.03.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

enum NotificationType: String, Codable {
    
    case rating
    case orderInfo
    
}
