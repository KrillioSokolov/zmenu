//
//  RemoteNotificationHandler.swift
//  Menu
//
//  Created by Kirill Sokolov on 06.03.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol DeepLinkScreenOpener: class {
    
//    func handleRemoteNotification(_ remoteNotificationHandler: RemoteNotificationHandler, handleMessageNotification networkMsgPush: MsgData)
    
}

public class RemoteNotificationHandler: NSObject {
    
    private let dispatcher: Dispatcher
    
    // MARK: - Public function
    
    init(dispatcher: Dispatcher) {
        self.dispatcher = dispatcher
    }
    
    @objc public func handleRemoteNotification(_ userInfo: [String : Any]) {
        
        guard let userInfoData = userInfo[NotificationConstants.Keys.data] as? [String : Any],
            let action = userInfoData[NotificationConstants.Keys.action] as? String
             else { return } //let networkMsgPush = makeNetworkMsgPush(data: data)
        
        let isKnownType = isRemoteNotificationOfKnownAction(action) // check if push is of known type
        //let isAuthorizedUser = UserSession.userIsAuthorized() // no need to display push is user isn't loginned

        if isKnownType { //isAuthorizedUser &&
            if let data = try? JSONSerialization.data(withJSONObject: userInfoData, options: []), let notificationResponse = try? JSONDecoder().decode(RemoteNotificationResponse.self, from: data) {
                switch notificationResponse.action {
                case .rating:
                    let value = OrdersEvent.NavigationEvent.RateMaster.Value(order: notificationResponse.order)
                    
                    dispatcher.dispatch(type: OrdersEvent.NavigationEvent.RateMaster.self, result: Result(value: value))
                case .orderInfo:
                    let value = OrdersEvent.NavigationEvent.OrderInfo.Value(animated: true)
                    
                    dispatcher.dispatch(type: OrdersEvent.NavigationEvent.OrderInfo.self, result: Result(value: value))
                }
            }
        }
    }
    
//    private func makeNetworkMsgPush(data: [String : Any]) -> MsgData? {
//        guard let jsonData = try? JSONSerialization.data(withJSONObject: data, options: []) else { return nil }
//        let networkMsgPush = try? JSONDecoder().decode(MsgData.self, from: jsonData)
//
//        return networkMsgPush
//    }

    // MARK: - Private functions

    fileprivate func isRemoteNotificationOfKnownAction(_ action: String) -> Bool {
        switch action {
        case NotificationType.orderInfo.rawValue, NotificationType.rating.rawValue:
            return true
        default:
            return false
        }
    }

}
