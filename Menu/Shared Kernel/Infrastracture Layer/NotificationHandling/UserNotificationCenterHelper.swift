//
//  UserNotificationCenterHelper.swift
//  Menu
//
//  Created by Kirill Sokolov on 06.03.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UserNotifications
import UserNotificationsUI

public class UserNotificationCenterHelper: NSObject {
    
    @available(iOS 10.0, *)
    @objc public func handleNotificationRequest(_ userInfo: [String : Any], delegate: UNUserNotificationCenterDelegate) {
        guard let content = self.remoteNotificationContent(userInfo) else { return }
        
        let request = UNNotificationRequest(
            identifier: self.notificationIdentifier(userInfo),
            content: content,
            trigger: nil
        )
        
        UNUserNotificationCenter.current().add(request)
        { error in
            UNUserNotificationCenter.current().delegate = delegate
            if error != nil {
                // handle here
            }
        }
    }
    
    @objc public func handleNotificationRequest(_ userInfo: [String : Any]) {
        guard let notification = self.makeLocalNotification(userInfo) else { return }
        
        UIApplication.shared.presentLocalNotificationNow(notification)
    }
    
    fileprivate func notificationIdentifier(_ userInfo: [String : Any]) -> String {
        if let aps = userInfo[NotificationConstants.Keys.aps] as? [String : Any],
            let data = aps[NotificationConstants.Keys.data] as? [String : Any],
            let action = data[NotificationConstants.Keys.action] as? String {
        
        switch action {
            case NotificationType.orderInfo.rawValue:
                return NotificationConstants.Identifiers.orderInfo
            case NotificationType.rating.rawValue:
                return NotificationConstants.Identifiers.rating
            default:
                break
            }
        }
        return NotificationConstants.Identifiers.orderInfo
    }
    
    @available(iOS 10.0, *)
    fileprivate func remoteNotificationContent(_ userInfo: [String: Any]) -> UNMutableNotificationContent? {
        guard let pushData = userInfo[NotificationConstants.Keys.aps] as? [String : Any] else { return nil }
        guard let alertString = pushData[NotificationConstants.Keys.alert] as? [String : Any] else { return nil }
        
        let title = alertString[NotificationConstants.Keys.title] as? String
        let body = alertString[NotificationConstants.Keys.body] as? String
        
        return NotificationContent(body: body ?? "", title: title, userInfo: userInfo)
    }
    
    fileprivate func makeLocalNotification(_ userInfo: [String: Any]) -> UILocalNotification? {
        guard let pushData = userInfo[NotificationConstants.Keys.aps] as? [String : Any] else { return nil }
        guard let alertString = pushData[NotificationConstants.Keys.alert] as? [String : Any] else { return nil }

        let title = alertString[NotificationConstants.Keys.title] as? String
        let body = alertString[NotificationConstants.Keys.body] as? String
        let notification = UILocalNotification()

        notification.alertBody = body
        notification.alertAction = title
        notification.soundName = UILocalNotificationDefaultSoundName
        
        return notification
    }
    
}
