//
//  MenuDefaults.swift
//  Menu
//
//  Created by Kirill Sokolov on 14.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

import Foundation

final class MenuDefaults {
    
    let userDefaults = UserDefaults.standard
    
    func synchronize() {
        userDefaults.synchronize()
    }
    
    deinit {
        synchronize()
    }
    
    func get<T>( _ key: String ) -> T? {
        return userDefaults.value(forKey: key) as? T
    }
    
    func set(_ value: Any?, _ key: String) {
        if let _ = value {
            userDefaults.set(value, forKey: key)
        } else {
            userDefaults.removeObject(forKey: key)
        }
    }
}

extension MenuDefaults {
    
    var currentLanguage: String? {
        get { return get(#function) }
        set { set(newValue, #function) }
    }
    
    var AppleLanguages: [String]? {
        get { return get(#function) }
        set { set(newValue, #function)}
    }
    
}
