//
//  MenuUserKeyValueStorage.swift
//  MenuBussines
//
//  Created by Sokolov Kirill on 17.06.2018.
//  Copyright © 2018 Skaix. All rights reserved.
//

import Foundation
//import KeychainAccess

//KS: TODO: Commented code for keychain storage, uncomented when needed.

@objc public class MenuStorageFactory: NSObject {
    
    //Storages for specific user
    public static func makeUserDefaultsStorage(forUserId userId: String) -> MenuUserKeyValueStorage {
        return MenuUserKeyValueStorage(userId: userId, storage: UserDefaults.standard)
    }
    
//    public static func makeUserKeychainStorage(forUserId userId: String, service: String, keychainGroup: String) -> ChannelsUserKeyValueStorage {
//        let keychain = Keychain(service: service, accessGroup: keychainGroup).accessibility(.afterFirstUnlockThisDeviceOnly)    //we use this accessibility to avoid migration of data to a new device when restore backup
//        return ChannelsUserKeyValueStorage(userId: userId, storage: keychain)
//    }
    
    //User independent storages
//    public static func makeCommonKeychainStorage(forService service: String) -> Storage {
//        return Keychain(service: service)
//    }
    
}


extension UserDefaults: Storage {
    
    public func removeValue(forKey key: String) {
        removeObject(forKey: key)
    }    
    
    public func set(data: Data?, forKey key: String) {
        set(data, forKey: key)
    }
    
    public func set(string: String?, forKey key: String) {
        set(string, forKey: key)
    }
        
    //Common
    public var allKeys: [String] {
        return dictionaryRepresentation().keys.map { String($0) }
    }
    
}

//extension Keychain: Storage {
//
//    public func removeValue(forKey key: String) {
//        self[key] = nil
//    }
//
//    public func data(forKey key: String) -> Data? {
//        let data = try? self.getData(key)
//        return data as? Data
//    }
//
//    public func set(data: Data?, forKey key: String) {
//        if let value = data {
//            try? self.set(value, key: key)
//        } else {
//            self[key] = nil
//        }
//    }
//
//    public func string(forKey key: String) -> String? {
//        let string = try? self.getString(key)
//        return string as? String
//    }
//
//    public func set(string: String?, forKey key: String) {
//        if let string = string {
//            try? self.set(string, key: key)
//        } else {
//            self[key] = nil
//        }
//
//    }
//
//    public var allKeys: [String] {
//        return self.allKeys()
//    }
//
//}
