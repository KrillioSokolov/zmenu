//
//  MenuUserKeyValueStorage.swift
//  MenuBussines
//
//  Created by Sokolov Kirill on 17.06.2018.
//  Copyright © 2018 Skaix. All rights reserved.
//

import Foundation

@objc public class MenuUserKeyValueStorage: NSObject, Storage {
    
    private let prefix = "MenuBusiness"
    private let userId: String
    
    private let storage: Storage
    
    public init(userId: String, storage: Storage) {
        self.userId = userId
        self.storage = storage
    }
    
    //Data
    public func set(data: Data?, forKey key: String) {
        storage.set(data: data, forKey: internalKey(korKey: key))
    }
    
    public func data(forKey key: String) -> Data? {
        return storage.data(forKey:internalKey(korKey: key))
    }
    
    //String
    public func set(string: String?, forKey key: String) {
        storage.set(string: string, forKey: internalKey(korKey: key))
    }
    
    public func string(forKey key: String) -> String? {
        return storage.string(forKey:internalKey(korKey: key))
    }

    //Common
    public func removeValue(forKey key: String) {
        storage.removeValue(forKey: internalKey(korKey: key))
    }
    
    public var allKeys: [String] {
        return storage.allKeys
    }
    
    private func allMenuUserKeys() -> [String] {        //returns keys with a specific prefix in the start of each key
        let keyPrefix = prefix(forUserId: userId)
        return allKeys.filter { $0.starts(with: keyPrefix) }
    }
    
    public func allOriginalMenuUserKeys() -> [String] {    //return keys without specific prefix in the start of each key
        let keyPrefix = prefix(forUserId: userId)
        let keyLength = keyPrefix.count
        
        return allMenuUserKeys().map({ (string) -> String in
            let start = string.index(string.startIndex, offsetBy: keyLength)
            let end = string.endIndex
            return String(string[start..<end])
        })
    }
    
    //Keys
    private func internalKey(korKey key: String) -> String {
        return prefix(forUserId: userId) + key
    }
    
    private func prefix(forUserId userId: String) -> String {
        return prefix + "_" + userId + "_"
    }
    
    public func clearAllUserData() {
        let keys = allOriginalMenuUserKeys()
        keys.forEach { self.removeValue(forKey: $0) }
    }

}
