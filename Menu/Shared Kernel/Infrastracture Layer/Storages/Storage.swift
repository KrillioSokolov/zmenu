//
//  Storage.swift
//  MenuBusiness
//
//  Created by Kirill Sokolov on 14.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

public protocol Storage {
    
    //Data
    func data(forKey key: String) -> Data?
    func set(data: Data?, forKey key: String)
    
    //String
    func string(forKey key: String) -> String?
    func set(string: String?, forKey key: String)
    
    //Common
    func removeValue(forKey key: String)
    
    var allKeys: [String] { get }
    
}
