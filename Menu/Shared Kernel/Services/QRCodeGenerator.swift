//
//  File.swift
//  MenuBusiness
//
//  Created by Stas Chelak on 1/8/19.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

final class QRCodeGenerator {
    
    static func makeImage(from code: String, withSize size: CGSize) -> UIImage? {
        let data = code.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        
        guard let image = filter?.outputImage else { return nil }
        
        let scaleX = size.width / image.extent.size.width
        let scaleY = size.height / image.extent.size.height
        let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
        
        return UIImage(ciImage: image.transformed(by: transform))
    }
    
}
