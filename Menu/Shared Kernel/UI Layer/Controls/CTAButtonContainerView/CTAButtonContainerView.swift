//
//  CTAButtonContainerView.swift
//  Menu
//
//  Created by Maxim Letushov on 10/17/17.
//  Copyright © 2017 Menu. All rights reserved.
//

import UIKit
import SnapKit
import TransitionButton
import CoreGraphics

final class CTAButtonContainerView: UIView {
    
    enum CTAButtonAnimationStyle: String {
        
        case normal
        case shake
        
    }
    
    var animationStyle: CTAButtonAnimationStyle = .normal
    
    var animationStyleValue: StopAnimationStyle {
        if let animationStyle = CTAButtonAnimationStyle(rawValue: animationStyle.rawValue) {
            switch animationStyle {
            case CTAButtonAnimationStyle.normal:
                return .normal
            case CTAButtonAnimationStyle.shake:
                return .shake
            }
        }

        return .normal
    }
    
    private var isAnimating = false
    
    private(set) var ctaButton: TransitionButton!
    private(set) var topDividerView: UIView?
    private(set) var ctaButtonLeftConstraint: Constraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }

    private struct Constants {
        
        static let staticHeight: CGFloat = 60.0
        static let ctaButtonHeight: CGFloat = 40.0
        static let sideInset: CGFloat = 10.0
        static let bottomInset: CGFloat = 10.0
        static let ctaButtonCornerRadius: CGFloat = 20.0
        
    }
    
    struct ButtonSpacingRule {
        
        static let left = 10
        static let right = -10
        static let top = 10
        static let bottom = -10
        
    }
    
    static var heightInCurrentDevice: CGFloat {
        return Constants.staticHeight
    }
    
    static var bottomInsetHiddenBehindSafeArea: CGFloat {
        if isIPhoneX {
            return CGFloat(15)
        }
        
        return 0
    }
    
    static var isIPhoneX: Bool {
        let iphoneXHeight = CGFloat(2436)
        
        return UIScreen.main.nativeBounds.height == iphoneXHeight
    }
    
    private func commonInit() {
        ctaButton = TransitionButton(type: .custom)
        ctaButton.spinnerColor = .white //OP: TODO: ask about proper color later
        
        addSubview(ctaButton)
        
        backgroundColor = .clear
        
        snp.updateConstraints({ (maker) in
            maker.height.equalTo(Constants.staticHeight)
        })
        
        ctaButton.snp.makeConstraints({ [weak self] (maker) in
            guard let strongSelf = self else { return }
            
            maker.left.equalTo(strongSelf).offset(ButtonSpacingRule.left)
            maker.right.equalTo(strongSelf).offset(ButtonSpacingRule.right)
            maker.top.equalTo(strongSelf).offset(ButtonSpacingRule.top)
            maker.bottom.equalTo(strongSelf).offset(ButtonSpacingRule.bottom)
        })
        
        ctaButton.cornerRadius = Constants.ctaButtonCornerRadius
        ctaButton.layer.masksToBounds = true
        topDividerView?.backgroundColor = .black
        
        ctaButton.clipsToBounds = true
    }
    
    func startAnimation() {
        if !isAnimating {
            isAnimating = true
            
            ctaButton.startAnimation()
        }
    }
    
    func stopAnimation(_ completion: @escaping (() -> Void) = {}) {
        if isAnimating {
            isAnimating = false
            
            ctaButton.stopAnimation(animationStyle: animationStyleValue, completion: {
                completion()
            })
        }
    }
    
}

protocol ContentHorizontalCentering {
    
    var leading: CGFloat { get set }
    
}

extension CTAButtonContainerView: ContentHorizontalCentering {
    
    var leading: CGFloat {
        get {
            if let constraint = ctaButtonLeftConstraint?.layoutConstraints[0] {
                return constraint.constant
            }
            
            return 0
        }
        set {
            ctaButtonLeftConstraint?.layoutConstraints[0].constant = newValue
        }
    }
    
}
