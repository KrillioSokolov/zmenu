//
//  Alert.swift
//  Menu
//
//  Created by Sokolov Kirill on 5/26/17.
//  Copyright © 2017 Menu. All rights reserved.
//

import UIKit

enum AlertStyle {
    
    case error, success, info
    
}

protocol Alert {
    
    func show(with style: AlertStyle,
              title: String?,
              subtitle: String?)
    
}
