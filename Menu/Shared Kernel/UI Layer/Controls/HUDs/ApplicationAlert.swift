//
//  ApplicationAlert.swift
//  Menu
//
//  Created by Sokolov Kirill on 5/26/17.
//  Copyright © 2017 Menu. All rights reserved.
//

import UIKit

final class ApplicationAlert: Alert, AlertDimension {
    
    private var applicationAlertView: ApplicationAlertView?
    private var closeBlock: ((ApplicationAlertView) -> ())?
    
    private struct Constants {
        
        static let animationDuration: TimeInterval = 0.3
        static let animationdelay: TimeInterval = 3
        static let alertViewOffsetY: CGFloat = 30
        
    }
    
    init() {
        closeBlock = { alertView in
            self.hide(alertView)
        }
        subscribeToDeviceOrientation()
    }
    
    deinit {
        unsubscribeToDeviceOrientation()
    }
    
    func orientationChanged(_ notification: Notification) {
        guard let applicationAlertView = self.applicationAlertView else { return }
        
        applicationAlertView.frame = CGRect(x: leading, y: Constants.alertViewOffsetY, width: width, height: applicationAlertView.bounds.height)
    }
    
    func show(with style: AlertStyle, title: String?, subtitle: String?) {
        DispatchQueue.updateUI {
            UIApplication.shared.windows.forEach({ window in
                let oldApplicationAlertView = window.viewWithTag(UIView.hudTag)
                let oldP24LoaderView = window.viewWithTag(UIView.hudTag)
                oldApplicationAlertView?.removeFromSuperview()
                oldP24LoaderView?.removeFromSuperview()
            })
            
            if let window = UIApplication.shared.keyWindow {
                
                self.applicationAlertView = ApplicationAlertView(frame: CGRect(x: self.leading, y: -Constants.alertViewOffsetY, width: self.width, height: 0))
                
                guard let applicationAlertView = self.applicationAlertView else { return }
                
                applicationAlertView.setAttributes(with: style, title: title, subtitle: subtitle)
                applicationAlertView.closeBlock = self.closeBlock
                applicationAlertView.tag = UIView.hudTag
                window.addSubview(applicationAlertView)
                
                UIView.animate(withDuration: Constants.animationDuration, animations: {
                    applicationAlertView.frame = CGRect(x: self.leading, y: Constants.alertViewOffsetY, width: self.width, height: applicationAlertView.frame.size.height)
                }, completion: { finished in
                    if finished {
                        DispatchQueue.delay(delay: Constants.animationdelay, closure: {
                            UIView.animate(withDuration: Constants.animationDuration, animations: {
                                applicationAlertView.frame = CGRect(x: self.leading, y: -applicationAlertView.frame.size.height, width: self.width, height: applicationAlertView.frame.size.height)
                            }, completion: { finished in
                                if finished {
                                    applicationAlertView.removeFromSuperview()
                                }
                            })
                        })
                    }
                })
            }
        }
    }
    
    private func hide(_ applicationAlertView: ApplicationAlertView?, delay: TimeInterval = 0) {
        UIView.animate(withDuration: Constants.animationDuration, delay: delay, options: .curveLinear, animations: {
            let height: CGFloat = applicationAlertView?.bounds.height ?? 0
            applicationAlertView?.frame = CGRect(x: self.leading, y: -height, width: self.width, height: height)
        }, completion: { _ in
            applicationAlertView?.removeFromSuperview()
        })
    }
    
}

