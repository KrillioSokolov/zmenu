//
//  ApplicationInternetAlert.swift
//  Menu
//
//  Created by Kirill Sokolov on 10/18/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import UIKit

//final class ApplicationInternetAlert: InternetAlert, AlertDimension {
//
//    private var window: UIWindow!
//    private var applicationInternetAlertView: ApplicationInternetAlertView?
//    private var closeBlock: ((ApplicationInternetAlertView) -> ())?
//
//    private var offsetY: CGFloat {
//        let rootViewController = UIApplication.topViewController(fromController: window.rootViewController)
//        var navigationBarHeight: CGFloat = 0
//        if let rootViewController = rootViewController, let navController = rootViewController.navigationController {
//            navigationBarHeight = navController.navigationBar.bounds.height + UIApplication.shared.statusBarFrame.height
//        }
//
//        return navigationBarHeight
//    }
//
//    private struct Constants {
//
//        static let internetAlertViewHeight: CGFloat = 40
//        static let animationDuration: TimeInterval = 0.3
//        static let hidingAnimationDelay: TimeInterval = 1
//
//    }
//
//    init() {
//        closeBlock = { alertView in
//            self.hide(alertView)
//        }
//
//        subscribeToDeviceOrientation()
//    }
//
//    deinit {
//        unsubscribeToDeviceOrientation()
//    }
//
//    func orientationChanged(_ notification: Notification) {
//        applicationInternetAlertView?.frame = CGRect(x: leading, y: offsetY, width: width, height: Constants.internetAlertViewHeight)
//    }
//
//    func show(on window: UIWindow, with style: InternetAlertStyle) {
//        self.window = window
//
//        if let oldApplicationInternetAlertView = window.viewWithTag(UIView.internetHudTag) as? ApplicationInternetAlertView {
//            applicationInternetAlertView = oldApplicationInternetAlertView
//        } else {
//            applicationInternetAlertView = ApplicationInternetAlertView(frame: CGRect(x: leading, y: -Constants.internetAlertViewHeight, width: width, height: Constants.internetAlertViewHeight))
//            applicationInternetAlertView!.tag = UIView.internetHudTag
//            applicationInternetAlertView!.closeBlock = closeBlock
//            window.addSubview(applicationInternetAlertView!)
//            UIView.animate(withDuration: Constants.animationDuration, animations: {
//                self.applicationInternetAlertView?.frame = CGRect(x: self.leading, y: self.offsetY, width: self.width, height: Constants.internetAlertViewHeight)
//            })
//        }
//
//        switch style {
//        case .noConnection:
//            self.applicationInternetAlertView?.setupWithNoInternetConnectionMode()
//        case .connecting:
//            self.applicationInternetAlertView?.setupWithInternetConnectingMode()
//        case .connected:
//            self.applicationInternetAlertView?.setupWithInternetConnectedMode()
//            hide(applicationInternetAlertView, delay: Constants.hidingAnimationDelay)
//        }
//    }
//
//    private func hide(_ applicationInternetAlertView: ApplicationInternetAlertView?, delay: TimeInterval = 0) {
//        UIView.animate(withDuration: Constants.animationDuration, delay: delay, options: .curveLinear, animations: {
//            applicationInternetAlertView?.frame = CGRect(x: self.leading, y: -Constants.internetAlertViewHeight, width: self.width, height: Constants.internetAlertViewHeight)
//            applicationInternetAlertView?.alpha = 0
//        }, completion: { _ in
//            applicationInternetAlertView?.removeFromSuperview()
//        })
//    }
//
//}
