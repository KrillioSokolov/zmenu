//
//  DesignTheme.swift
//  Menu
//
//  Created by Menu on 4/25/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import Foundation

@objc public enum DesignTheme: Int {
    
    case light
    case dark
    
}
