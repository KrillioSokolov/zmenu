//
//  DesignStyleGuideProviding.swift
//  Menu
//
//  Created by Menu on 4/25/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import Foundation

protocol DesignStyleGuideProviding: class {
    
    var designStyleGuide: DesignStyleGuide! { get }
    
}
