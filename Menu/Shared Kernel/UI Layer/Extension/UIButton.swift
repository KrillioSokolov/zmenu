//
//  UIButton.swift
//  Menu
//
//  Created by Sokolov Kirill on 6/4/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import UIKit

extension UIButton {
    
    func alignVertical(spacing: CGFloat = 15.0) {
        guard let imageSize = self.imageView?.image?.size,
            let text = self.titleLabel?.text,
            let font = self.titleLabel?.font else {
                return
        }
        
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0,
                                            left: -imageSize.width,
                                            bottom: -(imageSize.height + spacing),
                                            right: 0.0)
        
        let labelString = NSString(string: text)
        
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: font])
        
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing),
                                            left: 0.0,
                                            bottom: 0.0,
                                            right: -titleSize.width)
        
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0
        
        self.contentEdgeInsets = UIEdgeInsets(top: edgeOffset,
                                              left: 0.0,
                                              bottom: edgeOffset,
                                              right: 0.0)
    }
    
    func setPerfectCorners() {
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    func setBorder(_ color: UIColor, width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
}

