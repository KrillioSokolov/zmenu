//
//  UIImageView+SDWebImage.swift
//  Menu
//
//  Created by Sokolov Kirill on 2/2/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func download(image url: String, placeholderImage: UIImage?, showLoader: Bool = false) {
        guard let imageURL = URL(string: url) else {
            self.image = placeholderImage
            return
        }
        
        if showLoader {
            self.sd_setShowActivityIndicatorView(true)
            self.sd_setIndicatorStyle(.white)
        }
        
        self.sd_setImage(with: imageURL, placeholderImage: showLoader ? nil : placeholderImage) { [weak self] (image, error, _, _) in
            self?.image = image != nil ? image : placeholderImage
        }
    }
    
    func cancelCurrentImageLoad() {
        self.sd_cancelCurrentImageLoad()
    }
    
}
