//
//  UIScrollView.swift
//  Menu
//
//  Created by Sokolov Kirill on 2/5/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    func scrollToBottomIfNeeded(withIndentation indent: CGFloat = 0.0, animated: Bool = false) {
        guard bounds.size.height - contentInset.bottom - indent < contentSize.height else { return }
        
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom - indent)
        setContentOffset(bottomOffset, animated: animated)
    }
    
    var scrolledToTop: Bool {
        let topEdge = 0 - contentInset.top
        return contentOffset.y <= topEdge
    }
    
    var scrolledToBottom: Bool {
        let bottomEdge = contentSize.height + contentInset.bottom - bounds.height
        return contentOffset.y >= bottomEdge
    }
    
    func isExpectedScrollToTop(with targetContentOffset: UnsafeMutablePointer<CGPoint>) -> Bool {
        let topEdge = 0 - contentInset.top
        
        return targetContentOffset.pointee.y <= topEdge
    }
    
    func isExpectedScrollToBottom(with targetContentOffset: UnsafeMutablePointer<CGPoint>) -> Bool {
        let bottomEdge = contentSize.height + contentInset.bottom - bounds.height
        return targetContentOffset.pointee.y >= bottomEdge
    }

    
}

extension UIScrollView {
    
    func showEmptyListMessage(_ message: String, textColor: UIColor) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.bounds.size.width - 20, height: self.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = textColor
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 12)
        messageLabel.sizeToFit()
        
        if let `self` = self as? UITableView {
            self.backgroundView = messageLabel
            self.separatorStyle = .none
        } else if let `self` = self as? UICollectionView {
            self.backgroundView = messageLabel
        }
    }
}


extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }
}

extension UIRefreshControl {
    
    func beginRefreshingManually() {
        
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
        }
        
        beginRefreshing()
    }
    
}
