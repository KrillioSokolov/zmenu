//
//  UIViewController.swift
//  Menu
//
//  Created by Kirill Sokolov on 05.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

protocol StatusPresenting {
    
    func presentError(message: String)
    func presentError(withTitle title: String, subtitle: String)
    
    func presentSuccess(message: String)
    func presentSuccess(withTitle title: String, subtitle: String)
    
    func presentStatus(message: String)
    func presentStatus(withTitle title: String, subtitle: String)
    
}

extension UIViewController: StatusPresenting {
    
    func presentError(message: String) {
        ApplicationAlert().show(with: .error, title: nil, subtitle: message)
    }
    
    func presentError(withTitle title: String, subtitle: String) {
        ApplicationAlert().show(with: .error, title: title, subtitle: subtitle)
    }
    
    func presentSuccess(message: String) {
        ApplicationAlert().show(with: .success, title: nil, subtitle: message)
    }
    
    func presentSuccess(withTitle title: String, subtitle: String) {
        ApplicationAlert().show(with: .success, title: title, subtitle: subtitle)
    }
    
    func presentStatus(message: String) {
        ApplicationAlert().show(with: .info, title: nil, subtitle: message)
    }
    
    func presentStatus(withTitle title: String, subtitle: String) {
        ApplicationAlert().show(with: .info, title: title, subtitle: subtitle)
    }
    
}

extension UIViewController {
    
    var isModal: Bool {
        return presentingViewController != nil ||
            navigationController?.presentingViewController?.presentedViewController === navigationController ||
            tabBarController?.presentingViewController is UITabBarController
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}

extension UIViewController {
    
    var iPhoneModelXBottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            guard isIPhoneModelX else { return 0 }
            
            return CGFloat(34)
        }
        
        return 0
    }
    
    var isIPhoneModelX: Bool {
        let deviceHeight = UIScreen.main.nativeBounds.height
        let iphoneXandXSHeight = CGFloat(2436)
        let iphoneXrHeight = CGFloat(1792)
        let iphoneXsMaxHeight = CGFloat(2688)
        let iPhoneModelXHeights = [iphoneXandXSHeight, iphoneXrHeight, iphoneXsMaxHeight]
        
        return iPhoneModelXHeights.contains(deviceHeight)
    }
    
    var tabBarVisibleHeight: CGFloat {
        if isIPhoneModelX {
            return tabBarController?.tabBar.isHidden == false ? bottomLayoutGuide.length : iPhoneModelXBottomInset
        } else {
            return tabBarController?.tabBar.isHidden == false ? bottomLayoutGuide.length : 0
        }
    }
    
}

extension UIViewController: SpinnerPresenting {
    
    func showSpinner(message: String? = nil, animated: Bool = true, blockUI: Bool = true) {
        ApplicationSpinner().show(on: view, text: message, animated: animated, blockUI: blockUI)
    }
    
    func hideSpinner(animated: Bool = true) {
        ApplicationSpinner().hide(from: view, animated: animated)
    }
    
    var isSpinerPresented: Bool {
        return !view.subviews.filter{$0 is MBProgressHUD}.isEmpty
    }
    
}
