//
//  AlertDimension.swift
//  MenuBusiness
//
//  Created by Kirill Sokolov on 21.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation
import UIKit

protocol AlertDimension: class {
    
    var leading: CGFloat { get }
    var width: CGFloat { get }
    
    func subscribeToDeviceOrientation()
    func unsubscribeToDeviceOrientation()
    func orientationChanged(_ notification: Notification)
    
}

extension AlertDimension {
    
    private var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    var leading: CGFloat {
        if UIDevice.isPhone() {
            return 0
        } else {
            return CenterContainerViewOffsetService.offsetWith(width: screenWidth, isPortrait: UIApplication.shared.statusBarOrientation.isPortrait)
        }
    }
    
    var width: CGFloat {
        if UIDevice.isPhone() {
            return screenWidth
        } else {
            return 3 * leading
        }
    }
    
    func subscribeToDeviceOrientation() {
        NotificationCenter.default.addObserver(forName: UIDevice.orientationDidChangeNotification, object: nil, queue: nil) {  [weak self] notification in
            self?.orientationChanged(notification)
        }
    }
    
    func unsubscribeToDeviceOrientation() {
        NotificationCenter.default.removeObserver(self)
    }
    
}
