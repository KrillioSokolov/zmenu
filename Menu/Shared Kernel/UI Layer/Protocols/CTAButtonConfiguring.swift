//
//  CTAContainerViewSupporting.swift
//  Menu
//
//  Created by Sokolov Kirill on 12/20/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import Foundation
import UIKit

protocol CTAButtonConfiguring: class {
    
    func refreshCTAButton(_ button: UIButton, withStyleguide styleguide: DesignStyleGuide)  //ML: TODO: delete this method when we replace all CTAButtons with CTAButtonContainerView
    
    func refreshCTAButtonContainerView(_ container: CTAButtonContainerView, styleguide: DesignStyleGuide)
}

extension CTAButtonConfiguring {

    func refreshCTAButton(_ button: UIButton, withStyleguide styleguide: DesignStyleGuide) {
        button.setTitleColor(styleguide.CTAButtonTitleColor, for: .normal)
        button.titleLabel?.font = styleguide.mediumFont(ofSize: 17)
        button.setBackgroundImage(styleguide.normalCTAButtonBackgroundImage, for: .normal)
        button.setBackgroundImage(styleguide.disabledCTAButtonBackgroundImage, for: .disabled)
    }
    
    func refreshDeleteCTAButton(_ button: UIButton, withStyleguide styleguide: DesignStyleGuide) {
        button.setTitleColor(styleguide.CTAButtonTitleColor, for: .normal)
        button.titleLabel?.font = styleguide.mediumFont(ofSize: 17)
        button.setBackgroundImage(styleguide.normalDeleteCTAButtonBackgroundImage, for: .normal)
        button.setBackgroundImage(styleguide.disabledCTAButtonBackgroundImage, for: .disabled)
    }
    
    func refreshCTAButtonContainerView(_ container: CTAButtonContainerView, styleguide: DesignStyleGuide) {
        container.topDividerView?.backgroundColor = styleguide.dividerColor
        container.ctaButton.setTitleColor(styleguide.CTAButtonTitleColor, for: .normal)
        container.ctaButton.titleLabel?.font = styleguide.mediumFont(ofSize: 17)
        container.ctaButton.setBackgroundImage(styleguide.normalCTAButtonBackgroundImage, for: .normal)
        container.ctaButton.setBackgroundImage(styleguide.disabledCTAButtonBackgroundImage, for: .disabled)
        addGradientToContainer(container, styleguide: styleguide)
    }
    
    func refreshDeleteCTAButtonContainerView(_ container: CTAButtonContainerView, styleguide: DesignStyleGuide) {
        container.topDividerView?.backgroundColor = styleguide.dividerColor
        container.ctaButton.setTitleColor(styleguide.CTAButtonTitleColor, for: .normal)
        container.ctaButton.titleLabel?.font = styleguide.mediumFont(ofSize: 17)
        container.ctaButton.setBackgroundImage(styleguide.normalDeleteCTAButtonBackgroundImage, for: .normal)
        container.ctaButton.setBackgroundImage(styleguide.disabledCTAButtonBackgroundImage, for: .disabled)
        addGradientToContainer(container, styleguide: styleguide)
    }

    private func addGradientToContainer(_ container: CTAButtonContainerView, styleguide: DesignStyleGuide) {
        let startColor = styleguide.backgroundScreenColor.withAlphaComponent(1.0)
        let endColor = styleguide.backgroundScreenColor.withAlphaComponent(0.0)
        
        container.gradientBackground(from: startColor, to: endColor, direction: .bottomToTop)
    }
    
}
