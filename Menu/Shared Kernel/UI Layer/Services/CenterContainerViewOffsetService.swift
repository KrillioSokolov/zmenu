//
//  CenterContainerViewOffsetService.swift
//  Menu
//
//  Created by Kirill on 02.10.17.
//  Copyright © 2017 Menu. All rights reserved.
//

import Foundation
import CoreGraphics

final class CenterContainerViewOffsetService {
    
    static private let screenWidth: CGFloat = 768
    
    static func offsetWith(width: CGFloat, isPortrait: Bool) -> CGFloat {

        if isPortrait {
            if width < screenWidth {
                return 15
            } else {
                return width / 5
            }
        } else {
            return width / 5
        }
    }
}
