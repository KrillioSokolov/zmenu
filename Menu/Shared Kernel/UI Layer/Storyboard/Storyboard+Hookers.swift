//
//  Storyboard+Menu.swift
//  Menu
//
//  Created by Kirill Sokolov on 24.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

import UIKit

fileprivate enum MenuStoryboardControllerId: String {
    
    case restaurant = "RestaurantStoryboardControllerId"
    
}


