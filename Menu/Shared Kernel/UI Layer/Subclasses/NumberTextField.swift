//
//  NumberTextField.swift
//  Menu
//
//  Created by Stas Chelak on 2/5/19.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import UIKit
import Foundation

final class NumberTextField: UITextField {
    
    var cleanNumber: String {
        return (text ?? "").components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    var isValid: Bool {
        let regex = "^\\d{9}"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: cleanNumber)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.autocorrectionType = .no
        self.keyboardType = UIKeyboardType.phonePad
        super.delegate = self
    }
    
    func formattedPhoneNumber(_ phone: String) -> String {
        let cleanPhoneNumber = phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XX) XXX XX XX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask {
            if index == cleanPhoneNumber.endIndex {
                break
            }
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    func isBackSpace(_ string: String) -> Bool {
        guard let char = string.cString(using: .utf8) else { return false }
        
        let isBackSpace = strcmp(char, "\\b") == -92
        
        return isBackSpace
    }

    
}

extension NumberTextField: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField.text?.count ?? 0 < 14 || isBackSpace(string) else { return false }
        
        textField.text = formattedPhoneNumber(textField.text ?? "")
        
        return true
    }
    
}
