//
//  UITextViewZeroInsets.swift
//  Menu
//
//  Created by Kirill Sokolov on 10.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable final class UITextViewZeroInsets: UITextView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    fileprivate func setup() {
        textContainerInset = .zero
        textContainer.lineFragmentPadding = 0
    }
    
}
