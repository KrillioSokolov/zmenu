//
//  ApplicationAlertView.swift
//  Menu
//
//  Created by Kirill Sokolov on 10/17/18.
//  Copyright © 2018 Menu. All rights reserved.
//

import UIKit
import SnapKit

private struct LocalizedStringConstants {
    
    static var successTitle: String { return "Спасибо".localized() }
    static var errorTitle: String { return "Ошибка".localized() }
    
}

// KS: TODO: maybe in future need to pass designstyleguide for config view appearance

final class ApplicationAlertView: UIView {
    
    var closeBlock: ((ApplicationAlertView) ->())?

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let subview = viewFromNib()
        addSubview(subview)
        configureConstraints(subview)

        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let subview = viewFromNib()
        addSubview(subview)
        configureConstraints(subview)
        
        commonInit()
    }
    
    private func configureConstraints(_ subview: UIView) {
        subview.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }

    private func commonInit() {
        let swipeFromDownToUp = UISwipeGestureRecognizer(target: self, action: #selector(close))
        swipeFromDownToUp.direction = UISwipeGestureRecognizer.Direction.up
        addGestureRecognizer(swipeFromDownToUp)
        
        backgroundColor = .clear
        titleLabel.textColor = .white
        subtitleLabel.textColor = .white

        containerView.layer.masksToBounds = false
        containerView.layer.cornerRadius = 10
    }
    
    @objc func close(gesture: UIGestureRecognizer) {
        closeBlock?(self)
    }
    
    func setTitles(_ title: String?, subtitle: String?) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        
        setNeedsLayout()
        layoutIfNeeded()
        
        bounds = CGRect(x: 0, y: 0, width: bounds.width, height: containerView.bounds.height)

        containerView.layer.applySketchShadow(color: .white, alpha: 0.25, x: 0, y: 2, blur: 20, spread: 0)
    }
    
    func setAttributes(with style: AlertStyle, title: String?, subtitle: String?) {
        switch style {
        case .error:
            titleLabel.text = title ?? LocalizedStringConstants.errorTitle
            containerView.backgroundColor = UIColor(r: 255, g: 91, b: 92)
            imageView.image = UIImage(named: "error_status")
        case .info:
            titleLabel.text = title
            containerView.backgroundColor = UIColor(r: 141, g: 198, b: 65)  // KS: TODO: Maybe use different colors for .info
            imageView.image = nil
        case .success:
            titleLabel.text = title
            containerView.backgroundColor = UIColor(r: 141, g: 198, b: 65)
            imageView.image = UIImage(named: "smile")
        }
        
        subtitleLabel.text = subtitle
        
        setNeedsLayout()
        layoutIfNeeded()
        
        bounds = CGRect(x: 0, y: 0, width: bounds.width, height: containerView.bounds.height)
        
        containerView.layer.applySketchShadow(color: .white, alpha: 0.25, x: 0, y: 2, blur: 20, spread: 0)
    }
    
}
